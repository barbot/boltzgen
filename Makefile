dune:
	dune build @install

b64: dune
#	base64 < _build/install/default/lib/boltzgen/runtime/boltzgen__runtime.cma > boltzgen_runtime.cma.b64
#	base64 < _build/install/default/lib/boltzgen/runtime/boltzgen__runtime.cmi > boltzgen_runtime.cmi.b64
	base64 < _build/install/default/lib/boltzgen/boltzgen.cmi > boltzgen.cmi.b64
	base64 < _build/install/default/lib/boltzgen/boltzgen.cma > boltzgen.cma.b64
#	base64 < _build/install/default/lib/boltzgen/runtime/boltzgen__runtime__Math.cmi > Boltzgen__runtime__Math.cmi.b64
	
test:
	dune runtest --force

coverage:
	dune build --instrument-with bisect_ppx @install
	export BISECTLIB=`ocamlfind query bisect_ppx`"/common/bisect_common.cma "`ocamlfind query bisect_ppx`/runtime/bisect.cma
	export BISECT_FILE=`pwd`/bisect
	dune runtest --instrument-with bisect_ppx --force
	bisect-ppx-report html
	bisect-ppx-report summary

web:
	rm -rf website
	dune build web/gen_test_web.js web/gen_test_web2.js web/gen_dot.js
#	dune build web/gen_test_web2.js
	mkdir -p website
	cp web/index.html website
	cp web/gen_for_caseine.html website
	cp web/viz.html website	
	cp web/style.css website/style.css
	cp _build/default/web/gen_test_web.js website/gen_test_web.js
	cp _build/default/web/gen_test_web2.js website/gen_test_web2.js
	cp _build/default/web/gen_dot.js website
	cp web/viz.js website
	cp web/download.min.js website
 

web2:
	rm -rf website
	ocamlbuild -I web -use-ocamlfind gen_test_web.byte gen_test_lib.cma
	js_of_ocaml gen_test_web.byte
	mkdir website
	mv gen_test_web.js website/
	cp web/style.css website/
	cp web/index.html website/
	cp _build/gen_test_lib.cma website/


clean:
	dune clean
	rm -rf _coverage
	rm -rf website
	rm bisect*

.PHONY: clean dune all web
