Simple test on standard lib types

  $ cat >reference.ml <<EOF
  > let sum x y = x + y
  > let sum2 x y = x +. y
  > let app f x = f x
  > let app2 f x y = f x y
  > let rec fact n = if n < 1 then 1 else n * fact (n - 1)
  > let map = List.map
  > let concat l1 l2 = l1 @ l2
  > let greating x = "hello:"^x
  > let correct = List.fold_left
  > EOF

  $ cat >wrong.ml <<EOF
  > let sum x y = x - y
  > let sum2 = sum
  > let app f x = f x +. 1.0
  > let rec fact n = if n = 1 then n else n * fact (n - 1)
  > let map f l = []
  > let greating x = "hello "^x
  > let correct = List.fold_left
  > EOF

  $ boltzgen ""
  boltzgen: internal error, uncaught exception:
            Failure("No function defined")
            
  [125]

  $ boltzgen "val sum: int -> int->int" wrong.ml reference.ml --seed 42
  Fail to reach target 5. Range is ]2,2[ continue with z=1, size=2
  sum (0) ((-1)) = 1 instead of -1
  [1]

  $ boltzgen "val sum: natp -> int->int" wrong.ml --path "" -v 1 | sed -e "s%/[^ ]*/lib%OPAMPATH%g"
  Fail to reach target 5. Range is ]2,2[ continue with z=1, size=2
  Cannot find file /runtime.
  Boltzman -> obj:5 type:'int*int' : z:1 -> G:1 E:2

  $ boltzgen "val sum2: float -> float->float" wrong.ml reference.ml --seed 42 2>&1 | grep -o "ins not included" | head -n 1

  $ boltzgen "val app: ('a -> float) -> 'a -> float" wrong.ml reference.ml --seed 42
  Fail to reach target 5. Range is ]2,2[ continue with z=1, size=2
  Fail to reach target 5. Range is ]1,1[ continue with z=1, size=1
  app (fun_a 1) 0 = -12.1534657354 instead of -13.1534657354
  [1]

  $ boltzgen "val app2: ('a -> 'b -> int list) -> 'a -> 'b -> int list" reference.ml --seed 42
  Fail to reach target 5. Range is ]3,3[ continue with z=1, size=3
  app2 (fun_a 1) 0 1 = [6; -1; -13; 7; -9; 12; -8; 15; -2; -13; -18]
  app2 (fun_a 0) 0 0 = [-1; -10; -8; -14]
  app2 (fun_a 1) 2 2 = [-9; -20; -6; -13; -15; -2; 9]
  app2 (fun_a 0) 1 3 = []
  app2 (fun_a 0) 2 2 = [2; -7; -7; -13; 6; -16; -4; -20; 9; 19; -1; -14; -14; -9; -11; -12; -6; 18; -16; 15; 7; -13; 18; -3]
  app2 (fun_a 3) 4 2 = [18; -5]
  app2 (fun_a 3) 7 0 = [-14; -20; -2; -10; 19; 7; 3; 19; 9; -4; 15; -11]
  app2 (fun_a 4) 5 8 = [18; -12; 18; 7]
  app2 (fun_a 1) 4 8 = [-2; -2; 11; 17; -10; -17; 7; -14; 16; 4; -5; -11; 18; 3; 17; -18; 6; -8; 18; -7; 9; -14; 1; -18; -4; 13; -3; 0; 13; 9; -9; -2; -7; 4; 15; -8; 3; 12; -6; -8; 6; -1; 15; -8; 11; -18; 4; -3; -19; -16; 17; 2; -16]
  app2 (fun_a 5) 4 9 = [1]
  app2 (fun_a 3) 2 5 = [7]
  app2 (fun_a 0) 10 11 = [15; 6; 2; -12; -14; -8; -14; 3; -9]
  app2 (fun_a 0) 0 11 = [-1; 16; 15; -13; 15; 4; 5; -6; 1; -6; 6; -4; -7; 0; -10; -18; -20; 15; -18; 15; 1; 19; 12; -3; -14; -13; 14; 11; -16; 18; -11; 4; 18]
  app2 (fun_a 8) 7 5 = [9; 0; -11; 16]
  app2 (fun_a 6) 0 4 = [-6; -10; -19; 10; 17]
  app2 (fun_a 9) 3 12 = [-16; 19; 1; 5; 19; -10; 2; 18; -15; -14; -8; -18; -1; 13; -6; -15; 8; -9; -16; -20; 9; 11]
  app2 (fun_a 9) 10 9 = [5; -15]
  app2 (fun_a 11) 16 3 = [19; 4; 4]
  app2 (fun_a 3) 0 15 = [3; 12; -7; 17; 13; -9; -5; -19; 0; 4; -2]
  app2 (fun_a 3) 14 5 = [14; -10; -13]

  $ boltzgen "val fact: nat -> int" wrong.ml reference.ml --seed 42
  Fail to reach target 5. Range is ]1,1[ continue with z=1, size=1
  fact (0) = Exception occurs instead of 1
  [1]

  $ boltzgen "val map: ('a -> 'b) -> 'a list -> 'b list" wrong.ml reference.ml --seed 42
  Fail to reach target 5. Range is ]1,1[ continue with z=1, size=1
  map (fun_a 1) ([1; 1; 0]) = [] instead of [15; 15; 14]
  [1]

  $ boltzgen "val concat: 'a list -> 'a list -> 'a list" wrong.ml reference.ml --seed 42  2>&1 | grep -o "but not provided"
  but not provided

  $ boltzgen "val greating: id_string -> string" wrong.ml reference.ml --seed 42
  greating ("db") = "hello db" instead of "hello:db"
  [1]

  $ boltzgen "val correct: ('a -> 'b -> 'a) -> 'a -> 'b list -> 'a" wrong.ml reference.ml -b 10 --seed 42
  Fail to reach target 5. Range is ]1,1[ continue with z=1, size=1

  $ boltzgen "val greating: simple_string -> string" wrong.ml reference.ml --seed 42
  greating ("JY") = "hello JY" instead of "hello:JY"
  [1]

  $ boltzgen "val greating: simple_spaced_string -> string" wrong.ml reference.ml --seed 42
  greating ("  ") = "hello   " instead of "hello:  "
  [1]



  $ boltzgen "val test:  small_nat -> float list*char -> int" -n 5 --value-only -b 10 --seed 42
  (1,([11.4088; 8.36323],'n'))
  (3,([],'b'))
  (5,([(-2.46839); 4.23839],'G'))
  (8,([18.2912; (-14.7527); 15.4601],';'))
  (5,([5.13202; (-15.9974)],'X'))

  $ boltzgen "val f : int option -> unit" -n 0 -b 3 --value-only
  Fail to reach target 3. Range is ]1,2[ continue with z=1, size=1.5

  $ boltzgen "val f : int option -> unit" -n 0 -b 0.5 --value-only
  Fail to reach target 0.5. Closest is 1 for z=0 continue with z=1, size=1.5

Tests with custom types

  $ cat >reference.ml <<EOF
  > type 'a tree = Empty | Node of 'a tree * 'a * 'a tree
  > let rec size = function Empty -> 0 | Node (fg,_,fd) -> 1+(size fg) +(size fd)
  > let intfold (x,_) = List.fold_left (+) x
  > let rec flatten acc = function Empty -> acc | Node (fg,v,fd) -> flatten (v @ (flatten acc fg)) fd
  > let rec up = function Empty -> Empty | Node (fg,v,fd) -> Node (up fg, [v], up fd) 
  > EOF

  $ cat >wrong.ml <<EOF
  > type 'a tree = Empty | Node of 'a tree * 'a * 'a tree
  > let rec size = function Empty -> 0 | Node (fg,_,fd) -> 1+ (max (size fg) (size fd))
  > let rec flatten acc = function Empty -> acc | Node (fg,v,fd) -> v@acc
  > let intfold _ _ x = x
  > EOF

  $ boltzgen "type 'a tree = Empty | Node of 'a tree * 'a * 'a tree val size: 'a tree -> int" -b 10 --seed 42
  open Boltzgen.Runtime
  let _ = set_max_size 10;
  set_boltzmann_size 10.000000;;
  module type EXPSIG = sig
  	type 'a tree = Empty | Node of 'a tree*'a*'a tree
  	val size : 'a tree -> int
  end
  module TestFunctor (R : EXPSIG ) = struct
  	open R
  	let to_string = string_of_int
         let _ = eval_typedef "type 'a tree = Empty | Node of 'a tree*'a*'a tree"
  	let _ =
  		prerr_endline ("size (Empty) = "^(try to_string (size (Empty)) with x -> Printexc.to_string x)^"");
  		prerr_endline ("size (Node(Node(Node(Empty,0,Empty),0,Empty),0,Empty)) = "^(try to_string (size (Node(Node(Node(Empty,0,Empty),0,Empty),0,Empty))) with x -> Printexc.to_string x)^"");
  		prerr_endline ("size (Node(Node(Empty,0,Empty),0,Node(Empty,0,Node(Empty,1,Empty)))) = "^(try to_string (size (Node(Node(Empty,0,Empty),0,Node(Empty,0,Node(Empty,1,Empty))))) with x -> Printexc.to_string x)^"");
  		prerr_endline ("size (Node(Node(Empty,2,\n         Node(Node(Node(Empty,0,Node(Empty,0,Empty)),2,Empty),0,Empty)),\n    2,Empty)) = "^(try to_string (size (Node(Node(Empty,2,
           Node(Node(Node(Empty,0,Node(Empty,0,Empty)),2,Empty),0,Empty)),
      2,Empty))) with x -> Printexc.to_string x)^"");
  		prerr_endline ("size (Node(Empty,0,Node(Empty,1,Node(Empty,1,Node(Empty,2,Empty))))) = "^(try to_string (size (Node(Empty,0,Node(Empty,1,Node(Empty,1,Node(Empty,2,Empty)))))) with x -> Printexc.to_string x)^"");
  		prerr_endline ("size (Node(Empty,2,Node(Empty,2,Node(Empty,2,Node(Empty,1,Empty))))) = "^(try to_string (size (Node(Empty,2,Node(Empty,2,Node(Empty,2,Node(Empty,1,Empty)))))) with x -> Printexc.to_string x)^"");
  		prerr_endline ("size (Node(Node(Empty,2,Empty),3,Empty)) = "^(try to_string (size (Node(Node(Empty,2,Empty),3,Empty))) with x -> Printexc.to_string x)^"");
  		prerr_endline ("size (Node(Empty,1,\n    Node(Node(Node(Empty,1,Node(Empty,1,Empty)),0,Node(Empty,0,Empty)),4,\n        Empty))) = "^(try to_string (size (Node(Empty,1,
      Node(Node(Node(Empty,1,Node(Empty,1,Empty)),0,Node(Empty,0,Empty)),4,
          Empty)))) with x -> Printexc.to_string x)^"");
  		prerr_endline ("size (Node(Empty,0,Empty)) = "^(try to_string (size (Node(Empty,0,Empty))) with x -> Printexc.to_string x)^"");
  		prerr_endline ("size (Node(Empty,4,Empty)) = "^(try to_string (size (Node(Empty,4,Empty))) with x -> Printexc.to_string x)^"");
  		prerr_endline ("size (Node(Empty,2,Node(Empty,1,Empty))) = "^(try to_string (size (Node(Empty,2,Node(Empty,1,Empty)))) with x -> Printexc.to_string x)^"");
  		prerr_endline ("size (Node(Empty,2,Empty)) = "^(try to_string (size (Node(Empty,2,Empty))) with x -> Printexc.to_string x)^"");
  		prerr_endline ("size (Node(Empty,3,\n    Node(Node(Empty,6,Node(Node(Empty,5,Empty),0,Empty)),3,\n        Node(Node(Empty,2,Node(Empty,3,Node(Node(Empty,4,Empty),0,Empty))),2,\n            Empty)))) = "^(try to_string (size (Node(Empty,3,
      Node(Node(Empty,6,Node(Node(Empty,5,Empty),0,Empty)),3,
          Node(Node(Empty,2,Node(Empty,3,Node(Node(Empty,4,Empty),0,Empty))),2,
              Empty))))) with x -> Printexc.to_string x)^"");
  		prerr_endline ("size (Node(Node(Node(Empty,5,Empty),2,Empty),5,\n    Node(Node(Node(Empty,6,Node(Node(Empty,4,Empty),2,Empty)),0,\n             Node(Node(Empty,5,Node(Empty,0,Empty)),4,Empty)),\n        3,Empty))) = "^(try to_string (size (Node(Node(Node(Empty,5,Empty),2,Empty),5,
      Node(Node(Node(Empty,6,Node(Node(Empty,4,Empty),2,Empty)),0,
               Node(Node(Empty,5,Node(Empty,0,Empty)),4,Empty)),
          3,Empty)))) with x -> Printexc.to_string x)^"");
  		prerr_endline ("size (Node(Node(Node(Node(Empty,1,Empty),6,Empty),7,Empty),1,Empty)) = "^(try to_string (size (Node(Node(Node(Node(Empty,1,Empty),6,Empty),7,Empty),1,Empty))) with x -> Printexc.to_string x)^"");
  		prerr_endline ("size (Node(Empty,1,Empty)) = "^(try to_string (size (Node(Empty,1,Empty))) with x -> Printexc.to_string x)^"");
  		prerr_endline ("size (Node(Empty,6,Node(Empty,7,Node(Empty,1,Node(Empty,7,Empty))))) = "^(try to_string (size (Node(Empty,6,Node(Empty,7,Node(Empty,1,Node(Empty,7,Empty)))))) with x -> Printexc.to_string x)^"");
  		prerr_endline ("size (Node(Empty,0,\n    Node(Empty,0,\n        Node(Empty,0,\n            Node(Node(Node(Node(Empty,1,Empty),4,Empty),0,Empty),0,Empty))))) = "^(try to_string (size (Node(Empty,0,
      Node(Empty,0,
          Node(Empty,0,
              Node(Node(Node(Node(Empty,1,Empty),4,Empty),0,Empty),0,Empty)))))) with x -> Printexc.to_string x)^"");
  		prerr_endline ("size (Node(Node(Empty,5,Empty),7,\n    Node(Empty,8,\n        Node(Node(Node(Empty,7,\n                      Node(Node(Empty,5,Empty),4,Node(Empty,5,Empty))),\n                 2,Node(Empty,1,Empty)),\n            2,Empty)))) = "^(try to_string (size (Node(Node(Empty,5,Empty),7,
      Node(Empty,8,
          Node(Node(Node(Empty,7,
                        Node(Node(Empty,5,Empty),4,Node(Empty,5,Empty))),
                   2,Node(Empty,1,Empty)),
              2,Empty))))) with x -> Printexc.to_string x)^"");
  		prerr_endline ("size (Node(Node(Node(Empty,3,Node(Empty,2,Node(Empty,8,Empty))),3,Empty),5,\n    Node(Node(Node(Node(Empty,6,Empty),9,Empty),3,Empty),0,Empty))) = "^(try to_string (size (Node(Node(Node(Empty,3,Node(Empty,2,Node(Empty,8,Empty))),3,Empty),5,
      Node(Node(Node(Node(Empty,6,Empty),9,Empty),3,Empty),0,Empty)))) with x -> Printexc.to_string x)^"");
  		()
  end;;
  #mod_use "rendu.ml"
  module TA = TestFunctor (Rendu);;

  $ boltzgen "type 'a tree = Empty | Node of 'a tree * 'a * 'a tree val up: 'a tree -> 'a list tree" -b 10 -n 2 --seed 42 reference.ml
  up (Empty) = Empty
  up (Node(Node(Node(Empty,2,Empty),8,Empty),14,Empty)) = Node(Node(Node(Empty, [2], Empty), [8], Empty), [14], Empty)

  $ boltzgen "type 'a tree = Empty | Node of 'a tree * 'a * 'a tree val size: 'a tree -> int" wrong.ml reference.ml -b 10 --seed 42
  size (Node(Node(Empty,1,Empty),0,Node(Empty,0,Node(Empty,2,Empty)))) = 3 instead of 4
  [1]

  $ boltzgen "type 'a tree = Empty | Node of 'a tree * 'a * 'a tree val flatten: string list -> string list tree -> string list" wrong.ml reference.ml -b 10 --seed 42
  flatten (["q"]) (Node(Node(Empty,[" "; "W"; "_"],
           Node(Empty,["9"; "5"; "x"; "f"; "%"; "7"; "Q"],Empty)),
      ["ix"],Empty)) = ["ix"; "q"] instead of ["ix"; "9"; "5"; "x"; "f"; "%"; "7"; "Q"; " "; "W"; "_"; "q"]
  [1]


  $ boltzgen "val intfold: (int*int) -> int list ->  int" --seed 42 -b 20 --gen-for-caseine --rejection 0.8
  boltzgen: internal error, uncaught exception:
            Failure("Correction file required")
            
  [125]
-n 3

  $ boltzgen "val intfold: (int*int) -> int list ->  int" --seed 42 -b 20 --gen-for-caseine --rejection 0.8 reference.ml -n 3
  --- Consigne -----------
  &Eacute;crire la fonction <code>intfold : int*int -> int list -> int</code> telle que <code>intfold a l</code> 
  --- Base -----------
  module type EXPSIG = sig
  	val intfold : int*int -> int list -> int
  end
  module R:EXPSIG = struct
  {{ANSWER}}
  end
  open Boltzgen.Runtime
  let _ = 
    send_warning := true;
    set_max_size 20;;
  open R
  let to_string_intfold = string_of_int
  
  ;;
  
  --- vpl_evaluate.cases -------
  
  Case = Boltzgen test 1
  input = to_string_intfold (intfold ((-6),5) ([(-8); 6; 6]))
  output = -2
  
  Case = Boltzgen test 2
  input = to_string_intfold (intfold (8,(-1)) ([(-14); 9; (-7); 10; (-13); 3; 8; (-2)]))
  output = 2
  
  Case = Boltzgen test 3
  input = to_string_intfold (intfold ((-12),8) ([]))
  output = -12
  


  $ cat >reference.ml <<EOF
  > let testfun f _ = f 1
  > type unaire = O | I of unaire
  > let rec to_int = function O -> 0 | I (u) -> 1+ to_int u
  > let test f i = to_int (f i)
  > let rec add x = function O -> x | I (u) -> add (I (x)) u 
  > EOF

  $ boltzgen "val toption: int option ->  int" --seed 42 -b 5 --value-only -n 5
  Fail to reach target 5. Range is ]1,2[ continue with z=1, size=1.5
  None
  None
  Some(14)
  Some(13)
  None


  $ boltzgen "val toption: int option ->  int" --seed 464562 -b 1.5 --value-only -n 5
  Some((-2))
  None
  Some((-16))
  None
  Some(2)

  $ boltzgen "val testfun:(int -> int list) -> int list -> int list" --seed 42 -b 10 -n 5 reference.ml
  testfun (fun_a 0) ([(-2); 2; 1]) = [-3; -7; 18; -9]
  testfun (fun_a 0) ([]) = [-3; -7; 18; -9]
  testfun (fun_a 9) ([]) = [-10]
  testfun (fun_a 9) ([(-12); (-12); 5; 1; (-13); (-13); (-4); (-15); 5]) = [-10]
  testfun (fun_a 15) ([(-19); (-12); (-19)]) = [13; -6; 9; 16; 18; 7; 12; -13; -14; 13; 3; -20; 13; 10; 14; 5; -13; 14; 7; 13; 6; 14; -6; 19]

  $ boltzgen "type unaire = O | I of unaire val to_int : unaire -> int" --seed 42 -n 5 -b 10 reference.ml
  to_int (I(I(I(I(I(I(I(I(I(I(I(I(I(I(I(I(I(I(I(I(I(I(I(O)))))))))))))))))))))))) = 23
  to_int (I(I(I(I(I(I(I(I(I(I(I(I(O))))))))))))) = 12
  to_int (I(I(I(I(I(I(I(O)))))))) = 7
  to_int (I(I(I(I(I(I(I(I(I(I(I(I(I(O)))))))))))))) = 13
  to_int (I(O)) = 1

  $ boltzgen "type unaire = O | I of unaire val add : unaire -> unaire -> unaire" --seed 42 -n 5 -b 10 reference.ml
  add (I(I(I(I(I(I(I(I(I(I(I(I(I(I(O))))))))))))))) (I(I(O))) = I(I(I(I(I(I(I(I(I(I(I(I(I(I(I(I(O))))))))))))))))
  add (I(I(I(O)))) (I(O)) = I(I(I(I(O))))
  add (I(I(I(I(I(I(I(I(I(O)))))))))) (I(I(O))) = I(I(I(I(I(I(I(I(I(I(I(O)))))))))))
  add (I(I(I(I(I(I(O))))))) (O) = I(I(I(I(I(I(O))))))
  add (I(I(I(I(I(I(I(I(O))))))))) (I(I(I(I(O))))) = I(I(I(I(I(I(I(I(I(I(I(I(O))))))))))))

  $ boltzgen "type unaire = O | I of unaire val test : (int -> unaire) -> int -> int" --seed 42 -n 5 -b 10 reference.ml
  Fail to reach target 10. Range is ]2,2[ continue with z=1, size=2
  test (fun_a 0) ((-5)) = 20
  test (fun_a 0) ((-6)) = 3
  test (fun_a 9) (3) = 8
  test (fun_a 9) ((-3)) = 18
  test (fun_a 15) ((-4)) = 13

  $ cat t.ml
  open Boltzgen.Runtime
  let _ = set_max_size 20;
  set_boltzmann_size 20.000000;;
  module type EXPSIG = sig
  	type unaire = O | I of unaire
  	val test : (int->unaire) -> int -> int
  end
  module TestFunctor (R : EXPSIG ) = struct
  	open R
  	let to_string = string_of_int
         let _ = eval_typedef "type unaire = O | I of unaire"
  let fun_a seed a = rand_fun "(int->unaire)" seed (a)
  
  	let _ =
  		print_endline ("test (fun_a 0) ((-5)) = "^(try to_string (test (fun_a 0) ((-5))) with x -> Printexc.to_string x)^"");
  		print_endline ("test (fun_a 0) ((-6)) = "^(try to_string (test (fun_a 0) ((-6))) with x -> Printexc.to_string x)^"");
  		print_endline ("test (fun_a 9) (3) = "^(try to_string (test (fun_a 9) (3)) with x -> Printexc.to_string x)^"");
  		print_endline ("test (fun_a 9) ((-3)) = "^(try to_string (test (fun_a 9) ((-3))) with x -> Printexc.to_string x)^"");
  		print_endline ("test (fun_a 15) ((-4)) = "^(try to_string (test (fun_a 15) ((-4))) with x -> Printexc.to_string x)^"");
  		()
  end;;
  #mod_use "reference.ml"
  module TA = TestFunctor (Reference);;

  $ boltzgen "type tvar = O | O2 | I of tvar | I2 of int*tvar | I3 of int option | O3  val to_int : tvar -> tvar" --seed 42 -n 10 --value-only -b 10
  I(I(I2(14,I2(13,I(I(I(I(I(I2(19,I(O)))))))))))
  I(I(I3(None)))
  I(I(I2((-15),I(I(I(I(I2((-19),O2))))))))
  I(I2((-18),I2((-5),I(I(I(O2))))))
  I(I(I(I2((-6),I(I2((-7),I(I(I(I(I(I(I(I2((-3),I3(Some(11))))))))))))))))
  I(I2(0,I2(13,I(I2(10,I(I2(5,I2(17,I2(12,O)))))))))
  I(I(I2(19,I(I2((-20),I2((-14),O))))))
  I2(3,I(O3))
  I(I2((-12),I(I(I(O)))))
  I(I(I(I2(0,I(I(I(O2)))))))



  $ boltzgen "val testarray : bool array -> unit" --seed 42 -n 5
  open Boltzgen.Runtime
  let _ = set_max_size 5;
  set_boltzmann_size 5.000000;;
  module type EXPSIG = sig
  	val testarray : bool array -> unit
  end
  module TestFunctor (R : EXPSIG ) = struct
  	open R
  	let to_string = (fun ()->"()")
         	let _ =
  		prerr_endline ("testarray ([|false|]) = "^(try to_string (testarray ([|false|])) with x -> Printexc.to_string x)^"");
  		prerr_endline ("testarray ([|false; true; true; true|]) = "^(try to_string (testarray ([|false; true; true; true|])) with x -> Printexc.to_string x)^"");
  		prerr_endline ("testarray ([||]) = "^(try to_string (testarray ([||])) with x -> Printexc.to_string x)^"");
  		prerr_endline ("testarray ([|false; true|]) = "^(try to_string (testarray ([|false; true|])) with x -> Printexc.to_string x)^"");
  		prerr_endline ("testarray ([|true; true|]) = "^(try to_string (testarray ([|true; true|])) with x -> Printexc.to_string x)^"");
  		()
  end;;
  #mod_use "rendu.ml"
  module TA = TestFunctor (Rendu);;

  $ boltzgen "type t = unit*nat*float*char*bool val testalias : t -> t" --seed 42 -n 5
  open Boltzgen.Runtime
  let _ = set_max_size 5;
  set_boltzmann_size 5.000000;;
  module type EXPSIG = sig
  	type t = unit*int*float*char*bool
  	val testalias : t -> t
  end
  module TestFunctor (R : EXPSIG ) = struct
  	open R
  	let to_string = (fun (a1,a2,a3,a4,a5) -> ("("^((fun ()->"()") a1)^", "^(string_of_int a2)^", "^(string_of_float a3)^", "^((fun c -> "'"^(Char.escaped c)^"'") a4)^", "^(string_of_bool a5)^")"))
         let _ = eval_typedef "type t = unit*int*float*char*bool"
  	let _ =
  		prerr_endline ("testalias (((),0,1.05138,'g',true)) = "^(try to_string (testalias (((),0,1.05138,'g',true))) with x -> Printexc.to_string x)^"");
  		prerr_endline ("testalias (((),2,2.02302,'<',false)) = "^(try to_string (testalias (((),2,2.02302,'<',false))) with x -> Printexc.to_string x)^"");
  		prerr_endline ("testalias (((),2,(-2.93986),'(',false)) = "^(try to_string (testalias (((),2,(-2.93986),'(',false))) with x -> Printexc.to_string x)^"");
  		prerr_endline ("testalias (((),0,0.148984,'z',true)) = "^(try to_string (testalias (((),0,0.148984,'z',true))) with x -> Printexc.to_string x)^"");
  		prerr_endline ("testalias (((),3,1.12406,'=',true)) = "^(try to_string (testalias (((),3,1.12406,'=',true))) with x -> Printexc.to_string x)^"");
  		()
  end;;
  #mod_use "rendu.ml"
  module TA = TestFunctor (Rendu);;

  $ boltzgen "type t = E | N of t*nat*t val v:t" --seed 423112 -n 2 --value-only -b 30
  N(N(N(N(N(E,11,E),3,
         N(N(E,3,N(E,17,E)),3,
          N(N(N(N(E,1,N(N(E,0,E),5,N(E,7,E))),16,E),11,
             N(E,17,
              N(N(N(N(N(N(E,11,E),19,N(E,9,N(N(E,10,E),3,E))),12,
                     N(N(N(E,19,N(E,2,E)),1,E),17,N(E,0,E))),
                   5,E),
                 3,E),
               14,N(E,9,E)))),
           0,E))),
       12,E),
     16,N(E,14,N(N(N(E,11,N(E,5,N(N(E,8,E),8,E))),12,N(E,9,E)),19,E))),
   4,N(E,13,N(N(E,18,N(N(E,18,E),16,E)),6,E)))
  N(N(E,17,
     N(N(E,15,
        N(E,6,
         N(E,3,
          N(E,7,
           N(N(E,1,N(N(E,13,E),18,E)),14,
            N(N(N(E,17,N(E,3,E)),8,E),10,
             N(E,17,
              N(E,1,
               N(N(N(N(E,9,N(E,5,E)),4,E),6,N(N(E,7,E),18,N(E,8,E))),0,
                N(N(N(N(E,1,N(N(N(E,0,N(E,12,N(E,1,E))),9,E),15,E)),9,E),9,E),
                 3,N(E,5,E))))))))))),
      0,E)),
   9,N(E,18,N(E,18,N(E,12,N(N(E,16,E),0,E)))))


  $ boltzgen "type t = E | N of t*nat*t val v:t" --seed 423112 -n 2 --value-only -b 30 --gen-dot
  digraph V {
  
  node [shape=ellipse,label="N("] N0;
  node [shape=ellipse,label="N("] N1;
  N0 -> N1;//
  node [shape=ellipse,label="N("] N2;
  N1 -> N2;//
  node [shape=ellipse,label="N("] N3;
  N2 -> N3;//
  node [shape=ellipse,label="N("] N4;
  N3 -> N4;//
  node [shape=ellipse,label="E"] N5;
  N4 -> N5;//,
  node [shape=ellipse,label="11"] N6;
  N4 -> N6;//,
  node [shape=ellipse,label="E"] N7;
  N4 -> N7;//),
  node [shape=ellipse,label="3"] N8;
  N3 -> N8;//,
  node [shape=ellipse,label="N("] N9;
  N3 -> N9;//
  node [shape=ellipse,label="N("] N10;
  N9 -> N10;//
  node [shape=ellipse,label="E"] N11;
  N10 -> N11;//,
  node [shape=ellipse,label="3"] N12;
  N10 -> N12;//,
  node [shape=ellipse,label="N("] N13;
  N10 -> N13;//
  node [shape=ellipse,label="E"] N14;
  N13 -> N14;//,
  node [shape=ellipse,label="17"] N15;
  N13 -> N15;//,
  node [shape=ellipse,label="E"] N16;
  N13 -> N16;//)),
  node [shape=ellipse,label="3"] N17;
  N9 -> N17;//,
  node [shape=ellipse,label="N("] N18;
  N9 -> N18;//
  node [shape=ellipse,label="N("] N19;
  N18 -> N19;//
  node [shape=ellipse,label="N("] N20;
  N19 -> N20;//
  node [shape=ellipse,label="N("] N21;
  N20 -> N21;//
  node [shape=ellipse,label="E"] N22;
  N21 -> N22;//,
  node [shape=ellipse,label="1"] N23;
  N21 -> N23;//,
  node [shape=ellipse,label="N("] N24;
  N21 -> N24;//
  node [shape=ellipse,label="N("] N25;
  N24 -> N25;//
  node [shape=ellipse,label="E"] N26;
  N25 -> N26;//,
  node [shape=ellipse,label="0"] N27;
  N25 -> N27;//,
  node [shape=ellipse,label="E"] N28;
  N25 -> N28;//),
  node [shape=ellipse,label="5"] N29;
  N24 -> N29;//,
  node [shape=ellipse,label="N("] N30;
  N24 -> N30;//
  node [shape=ellipse,label="E"] N31;
  N30 -> N31;//,
  node [shape=ellipse,label="7"] N32;
  N30 -> N32;//,
  node [shape=ellipse,label="E"] N33;
  N30 -> N33;//))),
  node [shape=ellipse,label="16"] N34;
  N20 -> N34;//,
  node [shape=ellipse,label="E"] N35;
  N20 -> N35;//),
  node [shape=ellipse,label="11"] N36;
  N19 -> N36;//,
  node [shape=ellipse,label="N("] N37;
  N19 -> N37;//
  node [shape=ellipse,label="E"] N38;
  N37 -> N38;//,
  node [shape=ellipse,label="17"] N39;
  N37 -> N39;//,
  node [shape=ellipse,label="N("] N40;
  N37 -> N40;//
  node [shape=ellipse,label="N("] N41;
  N40 -> N41;//
  node [shape=ellipse,label="N("] N42;
  N41 -> N42;//
  node [shape=ellipse,label="N("] N43;
  N42 -> N43;//
  node [shape=ellipse,label="N("] N44;
  N43 -> N44;//
  node [shape=ellipse,label="N("] N45;
  N44 -> N45;//
  node [shape=ellipse,label="E"] N46;
  N45 -> N46;//,
  node [shape=ellipse,label="11"] N47;
  N45 -> N47;//,
  node [shape=ellipse,label="E"] N48;
  N45 -> N48;//),
  node [shape=ellipse,label="19"] N49;
  N44 -> N49;//,
  node [shape=ellipse,label="N("] N50;
  N44 -> N50;//
  node [shape=ellipse,label="E"] N51;
  N50 -> N51;//,
  node [shape=ellipse,label="9"] N52;
  N50 -> N52;//,
  node [shape=ellipse,label="N("] N53;
  N50 -> N53;//
  node [shape=ellipse,label="N("] N54;
  N53 -> N54;//
  node [shape=ellipse,label="E"] N55;
  N54 -> N55;//,
  node [shape=ellipse,label="10"] N56;
  N54 -> N56;//,
  node [shape=ellipse,label="E"] N57;
  N54 -> N57;//),
  node [shape=ellipse,label="3"] N58;
  N53 -> N58;//,
  node [shape=ellipse,label="E"] N59;
  N53 -> N59;//))),
  node [shape=ellipse,label="12"] N60;
  N43 -> N60;//,
  node [shape=ellipse,label="N("] N61;
  N43 -> N61;//
  node [shape=ellipse,label="N("] N62;
  N61 -> N62;//
  node [shape=ellipse,label="N("] N63;
  N62 -> N63;//
  node [shape=ellipse,label="E"] N64;
  N63 -> N64;//,
  node [shape=ellipse,label="19"] N65;
  N63 -> N65;//,
  node [shape=ellipse,label="N("] N66;
  N63 -> N66;//
  node [shape=ellipse,label="E"] N67;
  N66 -> N67;//,
  node [shape=ellipse,label="2"] N68;
  N66 -> N68;//,
  node [shape=ellipse,label="E"] N69;
  N66 -> N69;//)),
  node [shape=ellipse,label="1"] N70;
  N62 -> N70;//,
  node [shape=ellipse,label="E"] N71;
  N62 -> N71;//),
  node [shape=ellipse,label="17"] N72;
  N61 -> N72;//,
  node [shape=ellipse,label="N("] N73;
  N61 -> N73;//
  node [shape=ellipse,label="E"] N74;
  N73 -> N74;//,
  node [shape=ellipse,label="0"] N75;
  N73 -> N75;//,
  node [shape=ellipse,label="E"] N76;
  N73 -> N76;//))),
  node [shape=ellipse,label="5"] N77;
  N42 -> N77;//,
  node [shape=ellipse,label="E"] N78;
  N42 -> N78;//),
  node [shape=ellipse,label="3"] N79;
  N41 -> N79;//,
  node [shape=ellipse,label="E"] N80;
  N41 -> N80;//),
  node [shape=ellipse,label="14"] N81;
  N40 -> N81;//,
  node [shape=ellipse,label="N("] N82;
  N40 -> N82;//
  node [shape=ellipse,label="E"] N83;
  N82 -> N83;//,
  node [shape=ellipse,label="9"] N84;
  N82 -> N84;//,
  node [shape=ellipse,label="E"] N85;
  N82 -> N85;//)))),
  node [shape=ellipse,label="0"] N86;
  N18 -> N86;//,
  node [shape=ellipse,label="E"] N87;
  N18 -> N87;//))),
  node [shape=ellipse,label="12"] N88;
  N2 -> N88;//,
  node [shape=ellipse,label="E"] N89;
  N2 -> N89;//),
  node [shape=ellipse,label="16"] N90;
  N1 -> N90;//,
  node [shape=ellipse,label="N("] N91;
  N1 -> N91;//
  node [shape=ellipse,label="E"] N92;
  N91 -> N92;//,
  node [shape=ellipse,label="14"] N93;
  N91 -> N93;//,
  node [shape=ellipse,label="N("] N94;
  N91 -> N94;//
  node [shape=ellipse,label="N("] N95;
  N94 -> N95;//
  node [shape=ellipse,label="N("] N96;
  N95 -> N96;//
  node [shape=ellipse,label="E"] N97;
  N96 -> N97;//,
  node [shape=ellipse,label="11"] N98;
  N96 -> N98;//,
  node [shape=ellipse,label="N("] N99;
  N96 -> N99;//
  node [shape=ellipse,label="E"] N100;
  N99 -> N100;//,
  node [shape=ellipse,label="5"] N101;
  N99 -> N101;//,
  node [shape=ellipse,label="N("] N102;
  N99 -> N102;//
  node [shape=ellipse,label="N("] N103;
  N102 -> N103;//
  node [shape=ellipse,label="E"] N104;
  N103 -> N104;//,
  node [shape=ellipse,label="8"] N105;
  N103 -> N105;//,
  node [shape=ellipse,label="E"] N106;
  N103 -> N106;//),
  node [shape=ellipse,label="8"] N107;
  N102 -> N107;//,
  node [shape=ellipse,label="E"] N108;
  N102 -> N108;//))),
  node [shape=ellipse,label="12"] N109;
  N95 -> N109;//,
  node [shape=ellipse,label="N("] N110;
  N95 -> N110;//
  node [shape=ellipse,label="E"] N111;
  N110 -> N111;//,
  node [shape=ellipse,label="9"] N112;
  N110 -> N112;//,
  node [shape=ellipse,label="E"] N113;
  N110 -> N113;//)),
  node [shape=ellipse,label="19"] N114;
  N94 -> N114;//,
  node [shape=ellipse,label="E"] N115;
  N94 -> N115;//))),
  node [shape=ellipse,label="4"] N116;
  N0 -> N116;//,
  node [shape=ellipse,label="N("] N117;
  N0 -> N117;//
  node [shape=ellipse,label="E"] N118;
  N117 -> N118;//,
  node [shape=ellipse,label="13"] N119;
  N117 -> N119;//,
  node [shape=ellipse,label="N("] N120;
  N117 -> N120;//
  node [shape=ellipse,label="N("] N121;
  N120 -> N121;//
  node [shape=ellipse,label="E"] N122;
  N121 -> N122;//,
  node [shape=ellipse,label="18"] N123;
  N121 -> N123;//,
  node [shape=ellipse,label="N("] N124;
  N121 -> N124;//
  node [shape=ellipse,label="N("] N125;
  N124 -> N125;//
  node [shape=ellipse,label="E"] N126;
  N125 -> N126;//,
  node [shape=ellipse,label="18"] N127;
  N125 -> N127;//,
  node [shape=ellipse,label="E"] N128;
  N125 -> N128;//),
  node [shape=ellipse,label="16"] N129;
  N124 -> N129;//,
  node [shape=ellipse,label="E"] N130;
  N124 -> N130;//)),
  node [shape=ellipse,label="6"] N131;
  N120 -> N131;//,
  node [shape=ellipse,label="E"] N132;
  N120 -> N132;//)))
  
  node [shape=ellipse,label="N("] N133;
  node [shape=ellipse,label="N("] N134;
  N133 -> N134;//
  node [shape=ellipse,label="E"] N135;
  N134 -> N135;//,
  node [shape=ellipse,label="17"] N136;
  N134 -> N136;//,
  node [shape=ellipse,label="N("] N137;
  N134 -> N137;//
  node [shape=ellipse,label="N("] N138;
  N137 -> N138;//
  node [shape=ellipse,label="E"] N139;
  N138 -> N139;//,
  node [shape=ellipse,label="15"] N140;
  N138 -> N140;//,
  node [shape=ellipse,label="N("] N141;
  N138 -> N141;//
  node [shape=ellipse,label="E"] N142;
  N141 -> N142;//,
  node [shape=ellipse,label="6"] N143;
  N141 -> N143;//,
  node [shape=ellipse,label="N("] N144;
  N141 -> N144;//
  node [shape=ellipse,label="E"] N145;
  N144 -> N145;//,
  node [shape=ellipse,label="3"] N146;
  N144 -> N146;//,
  node [shape=ellipse,label="N("] N147;
  N144 -> N147;//
  node [shape=ellipse,label="E"] N148;
  N147 -> N148;//,
  node [shape=ellipse,label="7"] N149;
  N147 -> N149;//,
  node [shape=ellipse,label="N("] N150;
  N147 -> N150;//
  node [shape=ellipse,label="N("] N151;
  N150 -> N151;//
  node [shape=ellipse,label="E"] N152;
  N151 -> N152;//,
  node [shape=ellipse,label="1"] N153;
  N151 -> N153;//,
  node [shape=ellipse,label="N("] N154;
  N151 -> N154;//
  node [shape=ellipse,label="N("] N155;
  N154 -> N155;//
  node [shape=ellipse,label="E"] N156;
  N155 -> N156;//,
  node [shape=ellipse,label="13"] N157;
  N155 -> N157;//,
  node [shape=ellipse,label="E"] N158;
  N155 -> N158;//),
  node [shape=ellipse,label="18"] N159;
  N154 -> N159;//,
  node [shape=ellipse,label="E"] N160;
  N154 -> N160;//)),
  node [shape=ellipse,label="14"] N161;
  N150 -> N161;//,
  node [shape=ellipse,label="N("] N162;
  N150 -> N162;//
  node [shape=ellipse,label="N("] N163;
  N162 -> N163;//
  node [shape=ellipse,label="N("] N164;
  N163 -> N164;//
  node [shape=ellipse,label="E"] N165;
  N164 -> N165;//,
  node [shape=ellipse,label="17"] N166;
  N164 -> N166;//,
  node [shape=ellipse,label="N("] N167;
  N164 -> N167;//
  node [shape=ellipse,label="E"] N168;
  N167 -> N168;//,
  node [shape=ellipse,label="3"] N169;
  N167 -> N169;//,
  node [shape=ellipse,label="E"] N170;
  N167 -> N170;//)),
  node [shape=ellipse,label="8"] N171;
  N163 -> N171;//,
  node [shape=ellipse,label="E"] N172;
  N163 -> N172;//),
  node [shape=ellipse,label="10"] N173;
  N162 -> N173;//,
  node [shape=ellipse,label="N("] N174;
  N162 -> N174;//
  node [shape=ellipse,label="E"] N175;
  N174 -> N175;//,
  node [shape=ellipse,label="17"] N176;
  N174 -> N176;//,
  node [shape=ellipse,label="N("] N177;
  N174 -> N177;//
  node [shape=ellipse,label="E"] N178;
  N177 -> N178;//,
  node [shape=ellipse,label="1"] N179;
  N177 -> N179;//,
  node [shape=ellipse,label="N("] N180;
  N177 -> N180;//
  node [shape=ellipse,label="N("] N181;
  N180 -> N181;//
  node [shape=ellipse,label="N("] N182;
  N181 -> N182;//
  node [shape=ellipse,label="N("] N183;
  N182 -> N183;//
  node [shape=ellipse,label="E"] N184;
  N183 -> N184;//,
  node [shape=ellipse,label="9"] N185;
  N183 -> N185;//,
  node [shape=ellipse,label="N("] N186;
  N183 -> N186;//
  node [shape=ellipse,label="E"] N187;
  N186 -> N187;//,
  node [shape=ellipse,label="5"] N188;
  N186 -> N188;//,
  node [shape=ellipse,label="E"] N189;
  N186 -> N189;//)),
  node [shape=ellipse,label="4"] N190;
  N182 -> N190;//,
  node [shape=ellipse,label="E"] N191;
  N182 -> N191;//),
  node [shape=ellipse,label="6"] N192;
  N181 -> N192;//,
  node [shape=ellipse,label="N("] N193;
  N181 -> N193;//
  node [shape=ellipse,label="N("] N194;
  N193 -> N194;//
  node [shape=ellipse,label="E"] N195;
  N194 -> N195;//,
  node [shape=ellipse,label="7"] N196;
  N194 -> N196;//,
  node [shape=ellipse,label="E"] N197;
  N194 -> N197;//),
  node [shape=ellipse,label="18"] N198;
  N193 -> N198;//,
  node [shape=ellipse,label="N("] N199;
  N193 -> N199;//
  node [shape=ellipse,label="E"] N200;
  N199 -> N200;//,
  node [shape=ellipse,label="8"] N201;
  N199 -> N201;//,
  node [shape=ellipse,label="E"] N202;
  N199 -> N202;//))),
  node [shape=ellipse,label="0"] N203;
  N180 -> N203;//,
  node [shape=ellipse,label="N("] N204;
  N180 -> N204;//
  node [shape=ellipse,label="N("] N205;
  N204 -> N205;//
  node [shape=ellipse,label="N("] N206;
  N205 -> N206;//
  node [shape=ellipse,label="N("] N207;
  N206 -> N207;//
  node [shape=ellipse,label="E"] N208;
  N207 -> N208;//,
  node [shape=ellipse,label="1"] N209;
  N207 -> N209;//,
  node [shape=ellipse,label="N("] N210;
  N207 -> N210;//
  node [shape=ellipse,label="N("] N211;
  N210 -> N211;//
  node [shape=ellipse,label="N("] N212;
  N211 -> N212;//
  node [shape=ellipse,label="E"] N213;
  N212 -> N213;//,
  node [shape=ellipse,label="0"] N214;
  N212 -> N214;//,
  node [shape=ellipse,label="N("] N215;
  N212 -> N215;//
  node [shape=ellipse,label="E"] N216;
  N215 -> N216;//,
  node [shape=ellipse,label="12"] N217;
  N215 -> N217;//,
  node [shape=ellipse,label="N("] N218;
  N215 -> N218;//
  node [shape=ellipse,label="E"] N219;
  N218 -> N219;//,
  node [shape=ellipse,label="1"] N220;
  N218 -> N220;//,
  node [shape=ellipse,label="E"] N221;
  N218 -> N221;//))),
  node [shape=ellipse,label="9"] N222;
  N211 -> N222;//,
  node [shape=ellipse,label="E"] N223;
  N211 -> N223;//),
  node [shape=ellipse,label="15"] N224;
  N210 -> N224;//,
  node [shape=ellipse,label="E"] N225;
  N210 -> N225;//)),
  node [shape=ellipse,label="9"] N226;
  N206 -> N226;//,
  node [shape=ellipse,label="E"] N227;
  N206 -> N227;//),
  node [shape=ellipse,label="9"] N228;
  N205 -> N228;//,
  node [shape=ellipse,label="E"] N229;
  N205 -> N229;//),
  node [shape=ellipse,label="3"] N230;
  N204 -> N230;//,
  node [shape=ellipse,label="N("] N231;
  N204 -> N231;//
  node [shape=ellipse,label="E"] N232;
  N231 -> N232;//,
  node [shape=ellipse,label="5"] N233;
  N231 -> N233;//,
  node [shape=ellipse,label="E"] N234;
  N231 -> N234;//))))))))))),
  node [shape=ellipse,label="0"] N235;
  N137 -> N235;//,
  node [shape=ellipse,label="E"] N236;
  N137 -> N236;//)),
  node [shape=ellipse,label="9"] N237;
  N133 -> N237;//,
  node [shape=ellipse,label="N("] N238;
  N133 -> N238;//
  node [shape=ellipse,label="E"] N239;
  N238 -> N239;//,
  node [shape=ellipse,label="18"] N240;
  N238 -> N240;//,
  node [shape=ellipse,label="N("] N241;
  N238 -> N241;//
  node [shape=ellipse,label="E"] N242;
  N241 -> N242;//,
  node [shape=ellipse,label="18"] N243;
  N241 -> N243;//,
  node [shape=ellipse,label="N("] N244;
  N241 -> N244;//
  node [shape=ellipse,label="E"] N245;
  N244 -> N245;//,
  node [shape=ellipse,label="12"] N246;
  N244 -> N246;//,
  node [shape=ellipse,label="N("] N247;
  N244 -> N247;//
  node [shape=ellipse,label="N("] N248;
  N247 -> N248;//
  node [shape=ellipse,label="E"] N249;
  N248 -> N249;//,
  node [shape=ellipse,label="16"] N250;
  N248 -> N250;//,
  node [shape=ellipse,label="E"] N251;
  N248 -> N251;//),
  node [shape=ellipse,label="0"] N252;
  N247 -> N252;//,
  node [shape=ellipse,label="E"] N253;
  N247 -> N253;//)))))
  }

  $ cat >a.ml <<EOF
  > let print_list x f = List.fold_left (fun a b -> a^(f b))  "" x
  > EOF
  $ boltzgen "val print_list : 'a list -> ('a -> string) -> string " -b 30 --seed 423112 -n 5 --gen-for-caseine a.ml
  --- Consigne -----------
  &Eacute;crire la fonction <code>print_list : 'a list -> ('a->string) -> string</code> telle que <code>print_list l f</code> 
  --- Base -----------
  module type EXPSIG = sig
  	val print_list : 'a list -> ('a->string) -> string
  end
  module R:EXPSIG = struct
  {{ANSWER}}
  end
  open Boltzgen.Runtime
  let _ = 
    send_warning := true;
    set_max_size 20;;
  open R
  let to_string_print_list = (fun s ->"\""^s^"\"")
  
  let fun_a seed a = rand_fun "('a->string)" seed (a)
  ;;
  
  --- vpl_evaluate.cases -------
  
  Case = Boltzgen test 1
  input = to_string_print_list (print_list ([1; 4; 1; 0; 1; 4; 3; 3; 0; 0; 4; 2; 0; 1; 4; 1; 1]) (fun_a 0))
  output = "#$Bye{8R%qh6 `n/F!))b6E)5?G+du#$Bye{8R%qh6 `n/F!))-`R%z!{>pGa,Ji7RTVp5#$Bye{8R%qh6 `n/F!))b6E)5?G+duT}4j30W.0pb1Mr:%?p3KT}4j30W.0pb1Mr:%?p3K-`R%z!{>pGa,Ji7RTVp5-`R%z!{>pGa,Ji7RTVp5b6E)5?G+duZ7BF-`R%z!{>pGa,Ji7RTVp5#$Bye{8R%qh6 `n/F!))b6E)5?G+du#$Bye{8R%qh6 `n/F!))#$Bye{8R%qh6 `n/F!))"
  
  Case = Boltzgen test 2
  input = to_string_print_list (print_list ([3; 8]) (fun_a 0))
  output = "T}4j30W.0pb1Mr:%?p3Kjvp-#_"
  
  Case = Boltzgen test 3
  input = to_string_print_list (print_list ([10; 6; 6; 2; 3; 10; 7; 5; 11; 5; 9]) (fun_a 0))
  output = "Q(BL_?;XN65!=bBK`q0l,9M)9}65!=bBK`q0l,9M)9}Z7BFT}4j30W.0pb1Mr:%?p3KQ(BL_?;XNM4.?And2Z}ho_ ,9eQ,phe`8;g6=LDCl hM5Zbhe`8;g6=LDCl hM "
  
  Case = Boltzgen test 4
  input = to_string_print_list (print_list ([0; 4; 12; 10; 13; 14]) (fun_a 1))
  output = "[x=?+XSbK/FN=,] &X=;78*0{ywO3ZdHI7431PhDxZ}>FYcp`b!7ofO|){GQ<#S0}*"
  
  Case = Boltzgen test 5
  input = to_string_print_list (print_list ([18; 16; 1; 18; 18; 9; 5; 12]) (fun_a 8))
  output = "u7cL<O4kzssa[t)-ahj A$V#kh} o q/?v&gZu7cL<O4kzssa[t)-ahj u7cL<O4kzssa[t)-ahj F0:{Ffdu>L;1l01D){Q;iY1j<p U"
  
  $ cat >a.ml <<EOF
  > type 'a tree = Empty | Node of 'a tree * 'a * 'a tree
  > let rec sum f = function Empty -> 0 | Node (lhs,l,rhs) -> sum f lhs + (List.fold_left f 0 l) + sum f rhs
  > EOF
  $ cat >b.ml <<EOF
  > type 'a tree = Empty | Node of 'a tree * 'a * 'a tree
  > let rec sum f = function Empty -> 0 | Node (lhs,l,rhs) -> f (sum f lhs) ((List.fold_left f 0 l) + sum f rhs)
  > EOF
  $ boltzgen "type 'a tree = Empty | Node of 'a tree * 'a * 'a tree val sum: (int -> int -> nat) -> int list tree -> int" -b 40 --seed 423116 -n 6  a.ml b.ml
  Fail to reach target 5. Range is ]1,1[ continue with z=1, size=1
  sum (fun_a 1) (Node(Empty,[3; (-4); (-2)],Empty)) = 6 instead of 1
  [1]


  $ boltzgen "type 'a tree = Empty | Node of 'a tree * 'a * 'a tree val sum: (int -> int -> nat) -> int list tree -> int" -b 10 --gen-for-caseine --gen-xml --seed 423116 -n 6  a.ml
  Fail to reach target 20. Range is ]1,1[ continue with z=1, size=1
  Case = Boltzgen test 1\ninput = to_string_f (sum (fun_a 1) (Node(Empty,[3; (-4); (-2)],Empty)))\noutput = 6\n\nCase = Boltzgen test 2\ninput = to_string_f (sum (fun_a 6) (Node(Node(Node(Empty,[1],Empty),[],Empty),[],Node(Empty,[],Node(Node(Node(Empty,[],Empty),[],Empty),[6; 2; (-6)],Empty)))))\noutput = 14\n\nCase = Boltzgen test 3\ninput = to_string_f (sum (fun_a 0) (Node(Empty,[],Empty)))\noutput = 0\n\nCase = Boltzgen test 4\ninput = to_string_f (sum (fun_a 5) (Empty))\noutput = 0\n\nCase = Boltzgen test 5\ninput = to_string_f (sum (fun_a 12) (Empty))\noutput = 0\n\nCase = Boltzgen test 6\ninput = to_string_f (sum (fun_a 15) (Node(Node(Node(Node(Empty,[],Node(Empty,[(-20); 9],Node(Node(Empty,[],Empty),[(-9)],Empty))),[],Node(Empty,[(-3)],Empty)),[12],Empty),[13],Node(Node(Empty,[],Empty),[4],Empty))))\noutput = 43\n\n<?xml version="1.0"  encoding="UTF-8"?>
  <quiz>
    <question type="vplquestion">
  
      <name>
          <text>f</text>
      </name>
      <questiontext format="html">
          <text><![CDATA[]]></text>
      </questiontext>
      <generalfeedback format="html">
        <text></text>
      </generalfeedback>
      <defaultgrade>1</defaultgrade>
      <penalty>0</penalty>
      <hidden>0</hidden>
      <idnumber></idnumber>
      <templatevpl>42095</templatevpl>
      <templatelang>ocaml</templatelang>
      <templatecontext><![CDATA[module type EXPSIG = sig
  	type 'a tree = Empty | Node of 'a tree*'a*'a tree
  	val sum : (int->int->int) -> int list tree -> int
  end
  module R:EXPSIG = struct
  {{ANSWER}}
  end
  open Boltzgen.Runtime
  let _ = 
    send_warning := true;
    set_max_size 20;;
  open R
  let to_string_f = string_of_int
  let _ = eval_typedef "type 'a tree = Empty | Node of 'a tree*'a*'a tree"
  
  ;;
  ]]></templatecontext>
      <answertemplate></answertemplate>
      <teachercorrection><![CDATA[type 'a tree = Empty | Node of 'a tree * 'a * 'a tree
  let rec sum f = function Empty -> 0 | Node (lhs,l,rhs) -> sum f lhs + (List.fold_left f 0 l) + sum f rhs
  ]]></teachercorrection>
      <validateonsave>1</validateonsave>
      <execfiles><![CDATA[{"vpl_evaluate.cases":""}]]></execfiles>
       <precheckpreference>same</precheckpreference>
       <gradingmethod>0</gradingmethod>
  </question></quiz>
