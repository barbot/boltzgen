open Boltzgen

type 'a mal = F | N of 'a * 'a mal

let _ = Runtime.eval_typedef "type 'a mal = F | N of 'a * 'a mal"
let rec size = function F -> 0 | N (_, q) -> 1 + size q
let _ = Random.self_init ()
let n = size (Runtime.rand_value ~size:100.0 "int mal")
let _ = Printf.printf "size:%i\n" n
