Tests on record type  

  $ boltzgen "type t = { i:int; f:float } val f : t -> t " -n 5 --seed 42
  Fail to reach target 5. Range is ]2,2[ continue with z=1, size=2
  open Boltzgen.Runtime
  let _ = set_max_size 5;
  set_boltzmann_size 5.000000;;
  module type EXPSIG = sig
  	type t = {i : int ; f : float}
  	val f : t -> t
  end
  module TestFunctor (R : EXPSIG ) = struct
  	open R
  	let to_string = (fun r -> ("{"^(string_of_int r.i)^"; "^(string_of_float r.f)^"}"))
         let _ = eval_typedef "type t = {i : int ; f : float}"
  	let _ =
  		prerr_endline ("f ({i = 0; f = 1.05138}) = "^(try to_string (f ({i = 0; f = 1.05138})) with x -> Printexc.to_string x)^"");
  		prerr_endline ("f ({i = 0; f = 1.02885}) = "^(try to_string (f ({i = 0; f = 1.02885})) with x -> Printexc.to_string x)^"");
  		prerr_endline ("f ({i = 1; f = 1.25448}) = "^(try to_string (f ({i = 1; f = 1.25448})) with x -> Printexc.to_string x)^"");
  		prerr_endline ("f ({i = (-3); f = (-0.418815)}) = "^(try to_string (f ({i = (-3); f = (-0.418815)})) with x -> Printexc.to_string x)^"");
  		prerr_endline ("f ({i = (-3); f = (-3.78991)}) = "^(try to_string (f ({i = (-3); f = (-3.78991)})) with x -> Printexc.to_string x)^"");
  		()
  end;;
  #mod_use "rendu.ml"
  module TA = TestFunctor (Rendu);;



  $ boltzgen "type 'a t = { tu : unit ; ti: natp; tf: float; tc:char; tl: 'a list} val testrecord : int t -> string t" --seed 42 -n 5 -b 10
  open Boltzgen.Runtime
  let _ = set_max_size 10;
  set_boltzmann_size 10.000000;;
  module type EXPSIG = sig
  	type 'a t = {tu : unit ; ti : int ; tf : float ; tc : char ; tl : 'a list}
  	val testrecord : int t -> string t
  end
  module TestFunctor (R : EXPSIG ) = struct
  	open R
  	let to_string = (fun r -> ("{"^((fun ()->"()") r.tu)^"; "^(string_of_int r.ti)^"; "^(string_of_float r.tf)^"; "^((fun c -> "'"^(Char.escaped c)^"'") r.tc)^"; "^((fun s ->(List.fold_left (fun a b -> (if a <> "[" then a^"; " else "[")^((fun s ->"\""^s^"\"") b) ) "[" s)^"]") r.tl)^"}"))
         let _ = eval_typedef "type 'a t = {tu : unit ; ti : int ; tf : float ; tc : char ; tl : 'a list}"
  	let _ =
  		prerr_endline ("testrecord ({tu = (); ti = 1; tf = 1.57707; tc = 'g'; tl = [1; 2; (-1)]}) = "^(try to_string (testrecord ({tu = (); ti = 1; tf = 1.57707; tc = 'g'; tl = [1; 2; (-1)]})) with x -> Printexc.to_string x)^"");
  		prerr_endline ("testrecord ({tu = (); ti = 1; tf = 0.186231; tc = 'z'; tl = [(-2); (-5)]}) = "^(try to_string (testrecord ({tu = (); ti = 1; tf = 0.186231; tc = 'z'; tl = [(-2); (-5)]})) with x -> Printexc.to_string x)^"");
  		prerr_endline ("testrecord ({tu = (); ti = 2; tf = (-4.16507); tc = 'v'; tl = [(-2)]}) = "^(try to_string (testrecord ({tu = (); ti = 2; tf = (-4.16507); tc = 'v'; tl = [(-2)]})) with x -> Printexc.to_string x)^"");
  		prerr_endline ("testrecord ({tu = (); ti = 6; tf = (-1.73423); tc = '&'; tl = [6; (-2)]}) = "^(try to_string (testrecord ({tu = (); ti = 6; tf = (-1.73423); tc = '&'; tl = [6; (-2)]})) with x -> Printexc.to_string x)^"");
  		prerr_endline ("testrecord ({tu = (); ti = 1; tf = (-2.36465); tc = 'f'; tl = [3]}) = "^(try to_string (testrecord ({tu = (); ti = 1; tf = (-2.36465); tc = 'f'; tl = [3]})) with x -> Printexc.to_string x)^"");
  		()
  end;;
  #mod_use "rendu.ml"
  module TA = TestFunctor (Rendu);;
 

  $ boltzgen "type 'a t = { tl: 'a list} val testrecord : int option t -> string list t" --seed 42 -n 5
  open Boltzgen.Runtime
  let _ = set_max_size 5;
  set_boltzmann_size 5.000000;;
  module type EXPSIG = sig
  	type 'a t = {tl : 'a list}
  	val testrecord : int option t -> string list t
  end
  module TestFunctor (R : EXPSIG ) = struct
  	open R
  	let to_string = (fun r -> ("{"^((fun s ->(List.fold_left (fun a b -> (if a <> "[" then a^"; " else "[")^((fun s ->(List.fold_left (fun a b -> (if a <> "[" then a^"; " else "[")^((fun s ->"\""^s^"\"") b) ) "[" s)^"]") b) ) "[" s)^"]") r.tl)^"}"))
         let _ = eval_typedef "type 'a t = {tl : 'a list}"
  	let _ =
  		prerr_endline ("testrecord ({tl = [None; Some(0)]}) = "^(try to_string (testrecord ({tl = [None; Some(0)]})) with x -> Printexc.to_string x)^"");
  		prerr_endline ("testrecord ({tl = []}) = "^(try to_string (testrecord ({tl = []})) with x -> Printexc.to_string x)^"");
  		prerr_endline ("testrecord ({tl = [Some(1); Some(1)]}) = "^(try to_string (testrecord ({tl = [Some(1); Some(1)]})) with x -> Printexc.to_string x)^"");
  		prerr_endline ("testrecord ({tl = [None; None]}) = "^(try to_string (testrecord ({tl = [None; None]})) with x -> Printexc.to_string x)^"");
  		prerr_endline ("testrecord ({tl = [Some(0)]}) = "^(try to_string (testrecord ({tl = [Some(0)]})) with x -> Printexc.to_string x)^"");
  		()
  end;;
  #mod_use "rendu.ml"
  module TA = TestFunctor (Rendu);;
 
  $ cat >reference.ml <<EOF
  > type 'a t = { tl: 'a list}
  > let f r = { tl = List.map (function None -> "N" | Some i -> string_of_int i) r.tl}
  > EOF

  $ boltzgen "type 'a t = { tl: 'a list} val f : int option t -> string t" --seed 42 -n 5 reference.ml
  f ({tl = [None; Some(1)]}) = {["N"; "1"]}
  f ({tl = []}) = {[]}
  f ({tl = [Some((-8)); Some((-2))]}) = {["-8"; "-2"]}
  f ({tl = [None; None]}) = {["N"; "N"]}
  f ({tl = [Some((-15))]}) = {["-15"]}
