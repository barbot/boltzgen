\ProvidesClass{SlideClass}
\LoadClass{beamer}

\RequirePackage{tikz}
\RequirePackage{graphicx}
\RequirePackage{hyperref}
\RequirePackage{hyperref}
\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{pifont}
\RequirePackage{enumerate}
\RequirePackage{pdfsync}
\RequirePackage{listings}
\RequirePackage{multicol,colortbl}
\RequirePackage{enumitem}

\usetikzlibrary{arrows,decorations.markings,decorations.pathmorphing,decorations.pathreplacing,backgrounds,fit,petri,shapes,positioning,calc,arrows.meta,arrows}

\synctex=1

\definecolor{sh_comment}{rgb}{0.12, 0.38, 0.18 } %adjusted, in Eclipse: {0.25, 0.42, 0.30 } = #3F6A4D
\definecolor{sh_keyword}{rgb}{0.37, 0.08, 0.25}  % #5F1441
\definecolor{sh_string}{rgb}{0.06, 0.10, 0.98}

\lstset {
language=C,
 rulesepcolor=\color{white},
 showspaces=false,showtabs=false,tabsize=2,
 numberstyle=\none,
 basicstyle= \normalsize\ttfamily,
 stringstyle=\color{sh_string},
 keywordstyle = \color{sh_keyword}\bfseries,
 commentstyle=\color{sh_comment}\itshape,
 captionpos=b,
 xleftmargin=0.5cm, xrightmargin=0.5cm,
 lineskip=-0.3em,
 escapebegin={\lstsmallmath}, escapeend={\lstsmallmathend}
 morekeywords={String}
 inputencoding=utf8,
 extendedchars=true,
 literate={é}{{\'e}}1 {è}{{\`e}}1 {à}{{\`a}}1 {~}{{\~}}1,
 moredelim=**[is][\color{red}]{§}{§},
}

\lstset {
language=Java,
 rulesepcolor=\color{white},
 showspaces=false,showtabs=false,tabsize=2,
 numberstyle=\none,
 basicstyle= \normalsize\ttfamily,
 stringstyle=\color{sh_string},
 keywordstyle = \color{sh_keyword}\bfseries,
 commentstyle=\color{sh_comment}\itshape,
 captionpos=b,
 xleftmargin=0.5cm, xrightmargin=0.5cm,
 lineskip=-0.3em,
 escapebegin={\lstsmallmath}, escapeend={\lstsmallmathend}
 morekeywords={String}
 inputencoding=utf8,
 extendedchars=true,
 literate={é}{{\'e}}1 {è}{{\`e}}1 {à}{{\`a}}1 {~}{{$\approx$}}1,
 moredelim=**[is][\color{red}]{€}{€},
}

\lstset {
language=caml,
 rulesepcolor=\color{white},
 showspaces=false,showtabs=false,tabsize=2,
 numberstyle=\none,
 basicstyle= \small\ttfamily\color{col2},
 stringstyle=\color{sh_string},
 keywordstyle = \color{sh_keyword}\bfseries,
 commentstyle=\color{sh_comment}\itshape,
 captionpos=b,
 xleftmargin=0.1cm, xrightmargin=0.1cm,
 lineskip=-0.3em,
 escapebegin={\lstsmallmath}, escapeend={\lstsmallmathend}
 inputencoding=utf8,
 extendedchars=true,
 literate={é}{{\'e}}1 {è}{{\`e}}1 {à}{{\`a}}1 {~}{{$\sim{}$}}1,
 moredelim=**[is][\color{red}]{§}{§},
}


%%%%%%%% Color
\definecolor{acceptcol}{rgb}{0.0,0.65,0}
\definecolor{rejectcol}{rgb}{0.65,0.0,0}

%\definecolor{col1}{rgb}{1.0,0.5,0.0} %orange
%\definecolor{col2}{rgb}{1.0,0.0,0.5} %magenta
%\definecolor{col3}{rgb}{0.1,0.6,1.0} %cyan
%\definecolor{col4}{rgb}{0.0,0.0,1.0}

\definecolor{White}{RGB}{255,255,255}
\definecolor{Black}{RGB}{0,0,0}
\definecolor{DarkGrey}{RGB}{150,150,150}

\definecolor{col3}{RGB}{224,82,6} %E05206 rouge
\definecolor{col1}{RGB}{255,160,47} %FFA02F Saumon
\definecolor{col2}{RGB}{130,150,0} %829600 vert
\definecolor{col4}{RGB}{110,70,30} %6E461E brun


\pgfdeclarelayer{edgelayer}
\pgfdeclarelayer{nodelayer}
\pgfsetlayers{edgelayer,nodelayer,main}

\tikzstyle{none}=[inner sep=0pt]
\tikzstyle{state}=[rectangle,fill=White,draw=Black]
\tikzstyle{place}=[circle,fill=White,draw=Black]
\tikzstyle{transition}=[rectangle,fill=White,draw=Black,text width=1pt,inner sep = 1pt,text height=7pt]
\tikzstyle{bullet}=[circle,fill=Black,draw=Black,inner sep = 2pt]
\tikzstyle{arc}=[->,thick]
\tikzstyle{line}=[thick]
\tikzstyle{rectround}=[rectangle,fill=col4!30,rounded corners=2pt]
\tikzstyle{code}=[anchor=west]
\tikzstyle{brace}=[decorate,decoration={brace,amplitude=3.0pt}]

\usetheme{Boadilla} %BeamerDef
\makeatletter %BeamerDef
%\definecolor{beamer@blendedblue}{rgb}{255,160,47} %BeamerDef
%\definecolor{light}{rgb}{255,160,47} %BeamerDef
\definecolor{forframetitle}{rgb}{0.8,0.5,0.1} %BeamerDef
\setbeamercolor{title}{fg=black} %BeamerDef
\setbeamercolor{frametitle}{fg=forframetitle} %BeamerDef
\setbeamercolor{normal text}{fg=black} %BeamerDef
\setbeamercolor{alerted text}{fg=col3} %BeamerDef
\setbeamercolor{example text}{fg=col2} %BeamerDef
\setbeamercolor{structure}{fg=col4} %BeamerDef
\setbeamercolor{background canvas}{parent=normal text}  %BeamerDef
\setbeamercolor{background}{parent=background canvas} %BeamerDef
\setbeamertemplate{frametitle}[default][left] %BeamerDef
\setbeamertemplate{footline}
{
  \leavevmode%
  \hbox{%
   \begin{minipage}{\textwidth}
    \flushright
    \insertframenumber{} / \inserttotalframenumber\hspace*{1ex}
  \end{minipage}
  }
  \vskip0pt%
}
\makeatother %BeamerDef
\beamertemplatenavigationsymbolsempty %BeamerDef

\setlist[itemize]{label=\textbullet,leftmargin=*}

\newcommand{\ignore}[1]{}
\newcommand{\hl}[1]{\textcolor{rejectcol}{#1}}
\newcommand{\pro}[1]{\textcolor{acceptcol}{#1}}
\newcommand{\con}[1]{\textcolor{rejectcol}{#1}}

\newcommand{\Vrai}{\pro{\checkmark}}
\newcommand{\Faux}{\con{\ding{55}}}


\newenvironment{solutionblock}[1]{
   \setbeamercolor{block body}{bg=col2!20}
  \setbeamercolor{block title}{bg=col2!60}
  \begin{block}{#1}}{\end{block}}
\newenvironment{problemblock}[1]{
   \setbeamercolor{block body}{bg=col3!20}
  \setbeamercolor{block title}{bg=col3!60}
  \begin{block}{#1}}{\end{block}}

\newenvironment{itemblock}[1]{
  \begin{block}{#1}\setlist[itemize]{label=\textbullet,leftmargin=*} \itemize}{\enditemize\end{block}}

\newenvironment{algoblock}{
  \setbeamercolor{block body}{bg=col2!20}
  \setbeamercolor{block title}{bg=col2!60}
  \begin{block}{Algorithme}\setlist[itemize]{label=--,leftmargin=*} \itemize}{\enditemize\end{block}}

\lstnewenvironment{caml}{\lstset{language=caml}}{}
\newcommand{\co}[1]{\lstinline[language=caml]{#1}}
