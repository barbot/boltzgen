open Html
open Boltzgen

let string_of_print f a =
  f Format.str_formatter a;
  Format.flush_str_formatter ()

let boltz_obj = ref 5.0
let max_size = ref 100.0
let eval_ref = ref true
let gen_caseine = ref false
let out_content = ref ""
let out_xml = ref ""
let nb_test = ref 20

let build_str_formatter () =
  let err_buffer = Buffer.create 512 in
  let err_formatter = Format.formatter_of_buffer err_buffer in
  ( err_formatter,
    fun () ->
      Format.pp_print_flush err_formatter ();
      let s = Buffer.contents err_buffer in
      Buffer.clear err_buffer;
      s )

(*
let html_of_spec2 td func z =
  let randfun = Hashtbl.create 4 in
  let srand =  gen_random_fun_def randfun func in
  let l = test_list randfun 20 z 20 func in
  let ts = text (Format.sprintf "let to_string = %s\n%s" (gen_string_of_compo [] func.outtype) srand) in
  div ((p [ts]):: l)*)

let gen_header out_file ~out_err:_ _ _ sigs ts random_fun_def =
  Format.fprintf out_file
    "%a\n\
     open Boltzgen.Runtime\n\
    \     module TestFunctor (R : EXPSIG ) = struct\n\
     \topen R\n\
     \tlet to_string = %s\n\
     \tlet escape_str s =\n\
     \tif s.[0] = '\"' && s.[String.length s - 1] = '\"' then\n\
     \t\"\\\\\\\"\" ^ String.sub s 1 (String.length s - 2) ^ \"\\\\\\\"\"\n\
     \t else s\n\
     %a\tlet _ =\n"
    sigs () ts random_fun_def ()

let gen_case out_file ~out_err ?throw:_ ?canonize:_ s =
  Format.fprintf out_file
    "\t\tprint_endline (\"%s = \"^(try to_string (%s) with x ->  \n\
    \           Printexc.to_string x)^\"\");@." (String.escaped s) s

let string_of_spec up_base up_consigne td func z t =
  let buffer = Buffer.create 512 in
  let formatter = Format.formatter_of_buffer buffer in
  let reference = get_value_by_id "reference" () in
  Generator_loop.generic_loop
    (if !gen_caseine then Gen_for_caseine.gen_header !max_size ~print_base:false
     else Generator_loop.gen_header !max_size)
    (if !gen_caseine then Gen_for_caseine.gen_case else gen_case)
    (fun out_file _ ->
      Format.fprintf out_file
        "\t\t()\n\
         end;;\n\
         module Reference = struct@[<v 2> %s @]end\n\
         module TA = TestFunctor (Reference);;" reference)
    ~boltz_evaluated:(td, func, z) formatter !max_size !nb_test t ();
  up_base [ text @@ Scanf.unescaped @@ Gen_for_caseine.get_base () ];
  up_consigne [ text @@ Gen_for_caseine.get_consigne () ];
  let file_t = Buffer.contents buffer in
  (*Format.fprintf Format.str_formatter "%s" file_t;*)
  if !eval_ref then
    let _ = Js_of_ocaml_toplevel.JsooTop.use Format.str_formatter file_t in
    (*let _ = Js_of_ocaml_toplevel.JsooTop.execute true ~pp_code:Format.str_formatter Format.str_formatter file_t in*)
    Format.flush_str_formatter ()
  else file_t

let split_module s =
  let l = String.split_on_char ' ' s in
  let rec aux = function
    | [] -> ""
    | [ "end" ] -> ""
    | [ "end\n" ] -> ""
    | t :: q -> t ^ " " ^ aux q
  in
  let rec aux2 = function
    | [] -> ""
    | t :: q when t <> "sig" && t <> "sig\n" -> aux2 q
    | _ :: q -> aux q
  in
  aux2 l

let _ =
  Js_of_ocaml.Dom_html.window##.onload
  := Js_of_ocaml.Dom.handler @@ fun _ ->
     Js_of_ocaml_toplevel.JsooTop.initialize ();
     ignore
     @@ Js_of_ocaml_toplevel.JsooTop.use Format.str_formatter
          "open Boltzgen.Runtime";

     let err_formatter, flush_err = build_str_formatter () in

     Js_of_ocaml.Sys_js.set_channel_flusher stdout
       (Format.fprintf Format.str_formatter "%s");
     Js_of_ocaml.Sys_js.set_channel_flusher stderr
       (Format.fprintf err_formatter "%s");
     let init, result = get_by_id' "result" in
     let alert, alert_up = get_by_id' "alert" in
     let sigt, sig_up = get_by_id' "sigt" in
     let reference = get_value_by_id "reference" in
     let spec, spec_up = get_by_id' "funspec" in
     let _, base_up = get_by_id' "base" in
     let _, consigne_up = get_by_id' "consigne" in
     let spec_val = ref "" in
     let update () =
       result [];
       spec_up [];
       sig_up [];
       alert_up [];
       base_up [];
       consigne_up [];

       try
         let ref_val = reference () in
         let _ =
           let v = "module A = struct " ^ ref_val ^ " end;;" in
           Js_of_ocaml_toplevel.JsooTop.execute true Format.str_formatter v
         in
         let err = flush_err () in
         if err <> "" then (
           result [ text (Format.flush_str_formatter ()) ];
           alert_up [ text err ])
         else
           let out_form = Format.flush_str_formatter () in
           spec_val := split_module out_form;
           spec_up [ text !spec_val ]
       with x ->
         result [];
         sig_up [];
         alert_up [ text "Not Parsing : "; text (Printexc.to_string x) ]
     in
     let generate () =
       result [];
       sig_up [ text "..." ];
       alert_up [];
       Js_of_ocaml_toplevel.JsooTop.initialize ();
       ignore
       @@ Js_of_ocaml_toplevel.JsooTop.use Format.str_formatter
            "open Boltzgen.Runtime";
       try
         let td, funcopt = Runtime.parse_string !spec_val in
         let func =
           match funcopt with
           | None -> failwith "No defined function"
           | Some x -> x
         in
         List.iter Runtime.evaluate td;
         List.iter
           (fun t ->
             ignore
             @@ Js_of_ocaml_toplevel.JsooTop.use Format.std_formatter
                  (string_of_print Runtime.Type.pp_def t))
           td;
         let intype = Runtime.Type.Prod func.intypes in
         let z, _, _ = Runtime.compute_boltzman intype !boltz_obj in
         sig_up
           [
             text (string_of_print Runtime.Type.pp_func func);
             br ();
             text ("z=" ^ string_of_float z);
           ];
         out_content := string_of_spec base_up consigne_up td func z !spec_val;

         result [ text !out_content ];
         alert_up [];
         let xmlform, xmlup = build_str_formatter () in
         Gen_for_caseine.gen_xml
           ~vplid:(get_value_by_id "vplid" ())
           xmlform
           (fun f () ->
             let ref_val = reference () in
             Format.pp_print_string f ref_val)
           (fun f () -> Format.pp_print_string f !out_content);
         out_xml := xmlup ()
       with x ->
         (*result [];
           sig_up [];*)
         alert_up [ text "Not Parsing : "; text (Printexc.to_string x) ]
     in

     update ();
     generate ();
     let _ =
       get_by_id
         ~on_change:(fun b ->
           boltz_obj := float_of_string b;
           update ())
         "b_value"
       (*(string_of_float !boltz_obj);*)
     in
     let _ =
       get_by_id
         ~on_change:(fun nmax ->
           max_size := float_of_string nmax;
           update ())
         "max_val"
     in
     let _ = get_by_id ~on_change:(fun _ -> update ()) "funspec" in
     let _ = get_by_id ~on_change:(fun _ -> update ()) "reference" in
     let _ =
       get_by_id
         ~on_change:(fun v ->
           eval_ref := v = "true";
           update ())
         "eval_ref"
     in
     let _ =
       get_by_id
         ~on_change:(fun v ->
           gen_caseine := v = "true";
           update ())
         "gen_caseine"
     in
     let _ =
       get_by_id
         ~on_change:(fun v ->
           nb_test := int_of_string v;
           update ())
         "nb_test"
     in

     let _ = get_by_id ~on_change:(fun _ -> generate ()) "generate" in

     let _ =
       get_by_id
         ~on_change:(fun _ ->
           download !out_xml "question.xml" "application/xml")
         "downloadQuizz"
     in

     Js_of_ocaml.Js._false
