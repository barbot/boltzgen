open Html
open Boltzgen

(*
   let exec b ppf s =
     let lb = Lexing.from_string s in
     try
       List.iter
         (fun phr ->
           if not (Toploop.execute_phrase b ppf phr) then raise Exit)
         (!Toploop.parse_use_file lb)
     with
       | Exit -> ()
       | x    -> Errors.report_error ppf x


   let exec_phrase s =
     exec false Format.std_formatter ("Format.pp_print_string Format.str_formatter ("^ s ^")");
     Format.flush_str_formatter ()

   let _ =
     Toploop.initialize_toplevel_env ();
     Toploop.input_name := "";
     ignore @@ Toploop.use_silently Format.std_formatter "test.ml";
     let str =  exec_phrase "string_of_int (sum 5 3)" in
     Printf.printf "test:'%s'" str
*)

(*let test_list hash max z n func =
  let rs = random_state () in
  let l = ref [] in
    for i=1 to n do
      let j = 2+ (i*(max-2)/n) in
      let s = call_random hash rs j z func in
      let txt = Format.sprintf "print_endline (\"%s\ = \"^to_string (%s) );" (String.escaped s) s in
      l :=  (text txt):: br () :: !l
    done;
    List.rev !l*)

let string_of_print f a =
  f Format.str_formatter a;
  Format.flush_str_formatter ()

let boltz_obj = ref 5.0
let max_size = ref 100.0
let eval_ref = ref false
let gen_caseine = ref false
let out_content = ref ""

(*
let html_of_spec2 td func z =
  let randfun = Hashtbl.create 4 in
  let srand =  gen_random_fun_def randfun func in
  let l = test_list randfun 20 z 20 func in
  let ts = text (Format.sprintf "let to_string = %s\n%s" (gen_string_of_compo [] func.outtype) srand) in
  div ((p [ts]):: l)*)

let gen_header out_file ~out_err:_ _ _ sigs ts random_fun_def =
  Format.fprintf out_file
    "%a\n\
     open Boltzgen.Runtime\n\
    \     module TestFunctor (R : EXPSIG ) = struct\n\
     \topen R\n\
     \tlet to_string = %s\n\
     \tlet escape_str s =\n\
     \tif s.[0] = '\"' && s.[String.length s - 1] = '\"' then\n\
     \t\"\\\\\\\"\" ^ String.sub s 1 (String.length s - 2) ^ \"\\\\\\\"\"\n\
     \t else s\n\
     %a\tlet _ =\n"
    sigs () ts random_fun_def ()

let gen_case out_file ~out_err ?throw:_ ?canonize:_ s =
  Format.fprintf out_file
    "\t\tprint_endline (\"%s = \"^(try to_string (%s) with x ->  \n\
    \           Printexc.to_string x)^\"\");@." (String.escaped s) s

let store_eval = ref ""

let string_of_spec td func z t =
  let buffer = Buffer.create 100 in
  let formatter_tests = Format.formatter_of_buffer buffer in
  let reference = get_value_by_id "reference" () in
  Generator_loop.generic_loop
    (if !gen_caseine then
       Gen_for_caseine.gen_header ~print_base:(not !eval_ref) !max_size
     else Generator_loop.gen_header !max_size)
    (if !gen_caseine then Gen_for_caseine.gen_case else gen_case)
    (fun out_file _ ->
      Format.fprintf out_file
        "\t\t()\n\
         end;;\n\
         module Reference = struct@[<v 2> %a %s @]end\n\
         module TA = TestFunctor (Reference);;"
        (Format.pp_print_list (fun f t ->
             Format.fprintf f "%a@." Runtime.Type.pp_def t))
        td reference)
    ~boltz_evaluated:(td, func, z) formatter_tests !max_size 20 t ();
  let file_t = Buffer.contents buffer in

  (*Format.fprintf Format.str_formatter "%s" file_t;*)
  if !eval_ref then (
    let _ = Js_of_ocaml_toplevel.JsooTop.use Format.str_formatter file_t in
    store_eval := Format.flush_str_formatter ();
    if !gen_caseine then (
      Format.fprintf Format.str_formatter "%a%s"
        Gen_for_caseine.gen_consigne_base () !store_eval;
      Format.flush_str_formatter ())
    else !store_eval)
  else file_t

let gen_full_xml () =
  ignore @@ Format.flush_str_formatter ();
  let reference = get_value_by_id "reference" () in
  Gen_for_caseine.gen_xml Format.str_formatter
    (fun out () -> Format.pp_print_string out reference)
    (fun out () -> Format.pp_print_string out !store_eval);
  Format.flush_str_formatter ()

(*let string_of_spec td func z t =
  let buffer = Buffer.create 100 in
  let formatter = Format.formatter_of_buffer buffer in
  let reference = get_value_by_id "reference" () in
  generic_loop
    (fun out_file ~out_err:_ _ sigs ts _ ->
      (*Format.fprintf out_file "%s\n" sigs;
      Format.fprintf out_file "let to_string = %s\n\n" ts;*)
      Format.fprintf formatter "let to_string = %s;;\n%s;;@." ts reference)
    (fun out_file ~out_err:_ ?throw:_ ?canonize:_ s ->
       Format.fprintf formatter
      "\t\tprint_endline (\"%s = \"^(try to_string (%s) with x -> Printexc.to_string \
       x)^\"\");@."
      (String.escaped s) s)
    (fun _ _ -> ())
    ~boltz_evaluated:(td, func, z) formatter !max_size 20 t ();
  let file_t = Buffer.contents buffer in
  (*Format.fprintf Format.str_formatter "%s" file_t;*)
  if !eval_ref then
    let _ = Js_of_ocaml_toplevel.JsooTop.use Format.str_formatter file_t in
    Format.flush_str_formatter ()
  else file_t*)

let _ =
  Js_of_ocaml.Dom_html.window##.onload
  := Js_of_ocaml.Dom.handler @@ fun _ ->
     Js_of_ocaml_toplevel.JsooTop.initialize ();
     Js_of_ocaml.Sys_js.set_channel_flusher stdout
       (Format.fprintf Format.str_formatter "%s");

     let init, result = get_by_id' "result" in
     let alert, alert_up = get_by_id' "alert" in
     let sigt, sig_up = get_by_id' "sigt" in
     let spec = get_value_by_id "funspec" in
     let update () =
       result [];
       sig_up [ text "..." ];
       alert_up [];

       try
         let spec_value = spec () in
         let td, funcopt = Runtime.parse_string spec_value in
         let func =
           match funcopt with
           | None -> failwith "No defined function"
           | Some x -> x
         in
         List.iter Runtime.evaluate td;
         List.iter
           (fun t ->
             ignore
             @@ Js_of_ocaml_toplevel.JsooTop.use Format.std_formatter
                  (string_of_print Runtime.Type.pp_def t))
           td;
         let intype = Runtime.Type.Prod func.intypes in
         let z, _, _ = Runtime.compute_boltzman intype !boltz_obj in
         sig_up
           [
             text (string_of_print Runtime.Type.pp_func func);
             br ();
             text ("z=" ^ string_of_float z);
           ];
         out_content := string_of_spec td func z spec_value;
         result [ text !out_content ];
         alert_up []
       with x ->
         result [];
         sig_up [];
         alert_up [ text "Not Parsing : "; text (Printexc.to_string x) ]
     in
     update ();
     let _ =
       get_by_id
         ~on_change:(fun b ->
           boltz_obj := float_of_string b;
           update ())
         "b_value"
       (*(string_of_float !boltz_obj);*)
     in
     let _ =
       get_by_id
         ~on_change:(fun nmax ->
           max_size := float_of_string nmax;
           update ())
         "max_val"
     in
     let _ = get_by_id ~on_change:(fun _ -> update ()) "funspec" in
     let _ = get_by_id ~on_change:(fun _ -> update ()) "reference" in
     let _ =
       get_by_id
         ~on_change:(fun v ->
           eval_ref := v = "true";
           update ())
         "eval_ref"
     in
     let _ =
       get_by_id
         ~on_change:(fun v ->
           gen_caseine := v = "true";
           update ())
         "gen_caseine"
     in

     let _ =
       get_by_id
         ~on_change:(fun _ ->
           download (gen_full_xml ()) "question.xml" "application/xml")
         "downloadQuizz"
     in

     Js_of_ocaml.Js._false
