open Html
open Boltzgen

let string_of_print f a =
  f Format.str_formatter a;
  Format.flush_str_formatter ()

let boltz_obj = ref 10.0
let max_size = ref 100.0
let rejection = ref 0.8

let rec rejection_sampling rs i func z =
  let cv, size = Runtime.gen_from_compo rs (int_of_float !max_size) func z in
  if
    float size > !boltz_obj *. (1.0 -. !rejection)
    && float size < !boltz_obj *. (1.0 +. !rejection)
    || i = 0
  then (cv, size)
  else rejection_sampling rs (i - 1) func z

let string_of_spec td func z t =
  let open Format in
  let rs = Random.State.make_self_init () in
  let cv, size =
    rejection_sampling rs (if !rejection = 0.0 then 0 else 500) func z
  in

  let buffer1 = Buffer.create 100 in
  let formatter = formatter_of_buffer buffer1 in
  Runtime.print_from_compo func formatter cv;
  pp_print_newline formatter ();
  let str_rep = Buffer.contents buffer1 in

  let buffer2 = Buffer.create 100 in
  let formatter2 = formatter_of_buffer buffer2 in
  pp_set_formatter_stag_functions formatter2 Runtime.stagfun_struct;
  pp_set_tags formatter2 true;
  pp_set_geometry formatter2 ~max_indent:2 ~margin:max_int;
  Runtime.print_from_compo func formatter2 cv;
  pp_print_newline formatter2 ();
  (*gen_value ~boltz_evaluated:(td, func, z) formatter !max_size 1 t;*)
  let dot_rep = Buffer.contents buffer2 in
  let dot_rep_split = String.split_on_char '\n' dot_rep in
  let dot_rep2 =
    List.fold_left
      (fun acc v ->
        let v2 = String.trim v in
        let cond =
          if String.length v2 >= 4 then String.sub v2 0 4 = "node" else false
        in
        if cond then acc ^ "\n" ^ String.trim v else acc ^ String.trim v)
      "" dot_rep_split
  in

  (*Format.fprintf Format.str_formatter "%s" file_t;*)
  (size, str_rep, dot_rep2)

let _ =
  Js_of_ocaml.Dom_html.window##.onload
  := Js_of_ocaml.Dom.handler @@ fun _ ->
     Js_of_ocaml.Sys_js.set_channel_flusher stdout
       (Format.fprintf Format.str_formatter "%s");

     (let open Js_of_ocaml.Url in
      let args = Current.arguments in
      (match List.assoc_opt "funspec" args with
      | None -> ()
      | Some q -> set_value_by_id "funspec" q);
      match List.assoc_opt "rejection" args with
      | None -> ()
      | Some q -> set_value_by_id "rejection" q);

     let init, result = get_by_id' "result" in
     let alert, alert_up = get_by_id' "alert" in
     let sigt, sig_up = get_by_id' "sigt" in
     let spec = get_value_by_id "funspec" in
     let update () =
       result [];
       sig_up [ text "..." ];
       alert_up [];

       try
         let spec_value = spec () in
         let td, funcopt = Runtime.parse_string spec_value in
         let func =
           match funcopt with
           | None -> failwith "No defined function"
           | Some x -> x
         in
         List.iter Runtime.evaluate td;
         let outtype = func.outtype in
         let z, g, eE =
           Runtime.compute_boltzman ~silent:true outtype !boltz_obj
         in
         let size, str_rep, dot_rep = string_of_spec td outtype z spec_value in
         sig_up
           [
             text (string_of_print Runtime.Type.pp_func func);
             br ();
             text (Format.sprintf "Target:%g z:%g obtain:%g" !boltz_obj z eE);
             br ();
             text ("G(z) = " ^ string_of_float g);
             br ();
             text ("size = " ^ string_of_int size);
           ];
         let data = "digraph V {\n" ^ dot_rep ^ "\n}" in
         result [ text @@ str_rep ];
         (ignore
         @@ Js_of_ocaml.(
              Js.Unsafe.fun_call
                (Js.Unsafe.js_expr "UpdateGraphViz")
                [| Js.Unsafe.inject @@ Js.string data |]));

         alert_up []
       with x ->
         result [];
         sig_up [];
         alert_up [ text "Not Parsing : "; text (Printexc.to_string x) ]
     in
     update ();
     let _ =
       get_by_id
         ~on_change:(fun b ->
           boltz_obj := float_of_string b;
           update ())
         "b_value"
       (*(string_of_float !boltz_obj);*)
     in
     let _ =
       get_by_id
         ~on_change:(fun b ->
           rejection := float_of_string b;
           update ())
         "rejection"
       (*(string_of_float !boltz_obj);*)
     in
     let _, update_refresh = get_by_id' "refresh" in
     update_refresh [ button ~on_click:update [ text "refresh" ] ];
     (*let _ =
         get_by_id
           ~on_change:(fun nmax ->
             max_size := float_of_string nmax;
             update ())
           "max_val"
       in*)
     let _ = get_by_id ~on_change:(fun _ -> update ()) "funspec" in

     (*let _ =
         get_by_id
           ~on_change:(fun v ->
             eval_ref := v = "true";
             update ())
           "eval_ref"
       in
       let _ =
         get_by_id
           ~on_change:(fun v ->
             gen_caseine := v = "true";
             update ())
           "gen_caseine"
       in*)
     Js_of_ocaml.Js._false
