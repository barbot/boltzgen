open Boltzgen
open Cmdliner

(*ocamlcommon.cma *)
let load_path path =
  let bisect = Option.value ~default:"" (Sys.getenv_opt "BISECTLIB") in

  "unix.cma " ^ "-I +compiler-libs ocamlcommon.cma -I " ^ path
  ^ "/../lib/boltzgen -I " ^ path ^ "/../lib/boltzgen/runtime -I " ^ path
  ^ " -I " ^ path ^ "/runtime -I " ^ path ^ "/.boltzgen.objs/byte/ " ^ bisect
  ^ " "
(* ^ " ~/.opam/4.11.1/lib/bisect_ppx/common/bisect_common.cmxa \
    ~/.opam/4.11.1/lib/bisect_ppx/runtime/bisect.cmxa "*)

(*let _ = Toploop.initialize_toplevel_env ()*)

let gen_test funcspec boltzman exec reference max number_of_call v seed
    value_only iscaseine isxml gen_dot tsrangeerror path2 =
  let path =
    match path2 with
    | None -> Filename.dirname Sys.executable_name
    | Some p -> p
  in
  Runtime.Type.verbose := v;
  let td, funcopt = Runtime.parse_string funcspec in
  List.iter Runtime.evaluate td;
  let func =
    match funcopt with None -> failwith "No function defined" | Some x -> x
  in
  let intype =
    match func.intypes with
    | [] -> func.outtype
    | [ a ] -> a
    | l -> Runtime.Type.Prod l
  in
  let z, _, size = Runtime.compute_boltzman intype boltzman in
  let tsrange =
    match tsrangeerror with
    | None -> None
    | Some e ->
        Some
          (int_of_float ((1.0 -. e) *. size), int_of_float ((1.0 +. e) *. size))
  in

  let gen_t f =
    let fo_st = open_out "t.ml" in
    let fo = Format.formatter_of_out_channel fo_st in
    f fo (float_of_int max) number_of_call funcspec;
    Format.pp_print_flush fo ();
    close_out fo_st;

    let cmd = "ocaml " ^ load_path path ^ " boltzgen.cma t.ml" in
    if v > 1 then print_endline cmd;
    let x = Sys.command cmd in
    if x > 0 then exit x
  in

  (*let gen_t f =
      let buff = Buffer.create 1024 in
      let fo = Format.formatter_of_buffer buff in
      f fo (float_of_int max) number_of_call funcspec;
      Format.pp_print_flush fo ();
      ignore @@ Toploop.use_file Format.std_formatter (Buffer.contents buff);
      ()
    in*)
  (match seed with None -> Random.self_init () | Some s -> Random.init s);
  match
    (exec, reference, value_only || func.intypes = [], iscaseine || isxml)
  with
  | Some fi, None, _, true ->
      if isxml then
        Gen_for_caseine.(
          gen_xml Format.std_formatter
            (fun out () -> copy_file out fi)
            (fun _ () ->
              gen_t
                (gen_test2 ?tsrange ~boltz_evaluated:(td, func, z) ~ftotest:fi)))
      else
        gen_t
          (Gen_for_caseine.gen_test ?tsrange ~boltz_evaluated:(td, func, z)
             ~ftotest:fi)
  | _, _, _, true -> failwith "Correction file required"
  | _, _, true, _ when gen_dot ->
      let open Format in
      pp_set_formatter_stag_functions std_formatter Runtime.stagfun_struct;
      pp_set_tags std_formatter true;
      pp_set_geometry std_formatter ~max_indent:2 ~margin:max_int;
      printf "digraph V {@.%a}@."
        (fun f ->
          Generator_loop.gen_value ?tsrange ~boltz_evaluated:(td, intype, z) f
            (float_of_int max))
        number_of_call
  | _, _, true, _ ->
      Generator_loop.gen_value ?tsrange ~boltz_evaluated:(td, intype, z)
        Format.std_formatter (float_of_int max) number_of_call
  | Some fi, Some fr, _, _ ->
      gen_t
        (Generator_loop.gen_test_diff ?tsrange ~boltz_evaluated:(td, func, z) fi
           fr)
  (*ignore @@ Sys.command "echo \"#use \\\"t.ml\\\";;\" | ocaml -I _build gen_test_lib.cma"*)
  | Some fi, None, _, _ ->
      gen_t
        (Generator_loop.gen_test ?tsrange ~boltz_evaluated:(td, func, z)
           ~ftotest:fi)
  (*ignore @@ Sys.command "echo \"#use \\\"t.ml\\\";;\" | ocaml -I _build Generator_loop.cma"*)
  | None, _, _, _ ->
      Generator_loop.gen_test ?tsrange ~boltz_evaluated:(td, func, z)
        ~out_err:true Format.std_formatter boltzman number_of_call funcspec

let funspec =
  let doc = "Signature of a function to test" in
  Arg.(required & pos 0 (some string) None & info [] ~doc ~docv:"FUNSPEC")

let boltzman =
  let doc = "Typical term size as a float" in
  Arg.(value & opt float 5.0 & info [ "b" ] ~doc ~docv:"TERMSIZE")

let exec =
  let doc =
    "Implementation containing the implementation of the function to test"
  in
  Arg.(value & pos 1 (some file) None & info [] ~docv:"IMPL" ~doc)

let reference =
  let doc = "Implementation containing the reference implementation" in
  Arg.(value & pos 2 (some file) None & info [] ~docv:"REFERENCE" ~doc)

let max_size =
  let doc = "Maximum size" in
  Arg.(value & opt int 20 & info [ "max-size" ] ~doc ~docv:"SIZE")

let path =
  let doc = "runtime path" in
  Arg.(value & opt (some string) None & info [ "path" ] ~doc ~docv:"PATH")

let number_test =
  let doc = "Number of tests" in
  Arg.(value & opt int 20 & info [ "n" ] ~doc ~docv:"NUMBER")

let verbose =
  let doc = "Verbosity level" in
  Arg.(value & opt int 0 & info [ "v" ] ~doc ~docv:"VERBOSE")

let seed =
  let doc = "Seed of the RNG" in
  Arg.(value & opt (some int) None & info [ "seed" ] ~doc ~docv:"SEED")

let gen_for_caseine =
  let doc = "Generate test for Caseine" in
  Arg.(value & flag & info [ "gen-for-caseine" ] ~doc ~docv:"CASEINE")

let gen_dot =
  let doc = "Generate value in dot format" in
  Arg.(value & flag & info [ "gen-dot" ] ~doc ~docv:"DOT")

let gen_xml =
  let doc = "Generate test for Caseine in XML moodle format" in
  Arg.(value & flag & info [ "gen-xml" ] ~doc ~docv:"CASEINE")

let value_only =
  let doc = "only print generated value" in
  Arg.(value & flag & info [ "value-only" ] ~doc ~docv:"VONLY")

let rejection =
  let doc =
    "specify allowed variation in size, warning uses rejection sampling"
  in
  Arg.(
    value & opt (some float) None & info [ "rejection" ] ~doc ~docv:"REJECTION")

let cmd =
  let doc = "generate test based on boltzman sampling" in
  let man =
    [
      `S Manpage.s_description;
      `P "$(tname) generate tests following $(i,FUNSPEC).";
    ]
  in
  let info =
    Cmd.info "boltzgen" ~version:"v0.9.4" ~doc ~exits:Cmd.Exit.defaults ~man
  in
  Cmd.v info
    Term.(
      const gen_test $ funspec $ boltzman $ exec $ reference $ max_size
      $ number_test $ verbose $ seed $ value_only $ gen_for_caseine $ gen_xml
      $ gen_dot $ rejection $ path)

let () = Stdlib.exit @@ Cmd.eval cmd
