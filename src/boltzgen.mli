module Runtime : sig
  (** Boltzgen runtime library entry point*)

  type nat = int
  type natp = int
  type bits = int
  type small_nat = int
  type simple_string = string
  type simple_spaced_string = string
  type id_string = string

  module Type : sig
    val verbose : int ref

    (** A type for ocaml type*)
    type compo_type =
      | Name of string * compo_type list
          (** denote a [type expr] with arguments i.e. ['a list] *)
      | Abstract of string  (** denote a polymorphic type i.e. ['a] *)
      | Fun of compo_type list * compo_type
          (** denote a function i.e. ['a -> 'b] *)
      | Prod of compo_type list  (** denote a tupe i.e. [ 'a * 'b * ... 'z ] *)

    type func = {
      name : string;
      intypes : compo_type list;
      outtype : compo_type;
    }
    (** A type for named function *)

    val ano_func : func -> compo_type
    val deano_func : string -> compo_type -> func

    type sum_type_def = (string * compo_type option * float option) list

    type def_type_elem =
      | Sum of sum_type_def
      | Alias of compo_type
      | Record of (string * compo_type) list

    type def_type = string * string list * def_type_elem
    (** define a named type with name, list of argument and definition *)

    type hidden_type
    (** abstract type for any ocaml value *)

    (** {1 Pretty printer} *)

    type Format.stag +=
      | Named_type_stag of compo_type
      | Prod_stag of compo_type
      | Graph_stag

    val pp_compo : ?use_boltz_id:bool -> Format.formatter -> compo_type -> unit
    val pp_type_of_out : Format.formatter -> compo_type -> unit

    val pp_func :
      ?use_boltz_id:bool -> ?pval:bool -> Format.formatter -> func -> unit

    val pp_def : Format.formatter -> def_type -> unit
  end

  (** {1 Main generation function} *)
  val compute_boltzman :
    ?silent:bool -> Type.compo_type -> float -> float * float * float
  (** [compute_boltzman fd z] Evaluate the bolzman generating function for function signature [fd]
    using boltzman parameter [z] *)

  val gen_string_of_compo : Type.compo_type -> string
  (** Generate the to_string function for a type *)

  val gen_from_compo :
    Random.State.t -> int -> Type.compo_type -> float -> Type.hidden_type * int
  (** Generate a value for a type *)

  val print_from_compo :
    Type.compo_type -> Format.formatter -> Type.hidden_type -> unit
  (** Print a value given its type *)

  val parse_string : string -> Type.def_type list * Type.func option
  (** {1 Parsing and type generation} *)

  val evaluate : Type.def_type -> unit
  (** Build the type and add it to the type librairy *)

  val eval_typedef : string -> unit
  (** [eval_typedef s] parse and evaluate an ocaml type definition *)

  (** {1 Parameters of generation} *)

  val set_max_size : int -> unit
  val set_boltzmann_size : float -> unit
  val boltzmann_size : float ref
  val send_warning : bool ref

  type random_fun_table =
    (Type.compo_type list * Type.compo_type, char) Hashtbl.t

  val gen_random_fun_def :
    Format.formatter ->
    Type.def_type list ->
    random_fun_table ->
    Type.func ->
    unit
  (** Generate and print random functions with memoisation *)

  val random_state : unit -> Random.State.t
  (** Initialize a random generator state*)

  val call_random :
    ?tsrange:int * int ->
    ?max_iter:int ->
    random_fun_table ->
    Random.State.t ->
    int ->
    float ->
    Type.func ->
    string
  (** Generate and print a call for a function *)

  (** {1 Convenient function for runtime value generation} *)

  val stagfun_struct : Format.formatter_stag_functions
  val nb_test : int ref
  val nb_fail : int ref

  val print_value : Format.formatter -> string -> 'a -> unit
  (** HIGHLY UNSAFE: given a type description and a value, print it on the formatter
    undefined behaviours if type do not match, see {! print_from_compo} for a safe version*)

  val rand_value : ?seed:int -> ?size:float -> ?silent:bool -> string -> 'a
  (** [rand_fun ~seed ~size type ] return a value of type [type] 
    [seed] is a seed for the random generator, if ommited Random.bits is used.
    [size] control the size of the value, override [set_boltzmann_size]
    see {! gen_from_compo} for a safer version*)

  val rand_fun : ?size:float -> ?silent:bool -> string -> int -> 'a -> 'b
  (** [rand_fun ~seed ~size type arg] is a pure generic function, it generate a value of type [type] 
    [seed] and [arg] are parameters for the value, same as [rand_value] but make typer happy *)

  val rand_fun_alea :
    ?seed:int -> ?size:float -> ?silent:bool -> string -> 'a -> 'b
  (** [rand_fun ~seed ~size type arg] is a pure generic function, it generate a value of type [type] 
    [seed] and [arg] are parameters for the value, same as [rand_value] but make typer happy *)

  (** {1 Functions for runtime equality checking} *)

  val assert_equal :
    ?throw:bool ->
    ?err:bool ->
    ('a -> string) ->
    ('b -> string) ->
    string ->
    (unit -> 'a) ->
    (unit -> 'b) ->
    unit

  val assert_equal_arg :
    ?throw:bool ->
    ?err:bool ->
    ('a -> string) ->
    ('b -> string) ->
    ('c -> string) ->
    ('c -> 'a) ->
    ('c -> 'b) ->
    'c ->
    unit

  val assert_equal_string :
    ?err:bool -> string -> (unit -> string) -> (unit -> string) -> unit
end

module Generator_loop : sig
  val mname_of_string : string -> string

  val print_typedef : Format.formatter -> Runtime.Type.def_type list -> unit
  (** Print a type definition*)

  val print_sig :
    Format.formatter -> Runtime.Type.def_type list * Runtime.Type.func -> unit
  (** Print a module signature matching type and function signature *)

  (** Internal test generation function*)

  val gen_header :
    float ->
    Format.formatter ->
    out_err:bool ->
    int ->
    Runtime.Type.func ->
    (Format.formatter -> unit -> unit) ->
    string ->
    (Format.formatter -> unit -> unit) ->
    unit

  val generic_loop :
    (Format.formatter ->
    out_err:bool ->
    int ->
    Runtime.Type.func ->
    (Format.formatter -> unit -> unit) ->
    string ->
    (Format.formatter -> unit -> unit) ->
    unit) ->
    (Format.formatter ->
    out_err:bool ->
    ?throw:bool ->
    ?canonize:string ->
    string ->
    unit) ->
    (Format.formatter -> 'b -> unit) ->
    ?out_err:bool ->
    ?tsrange:int * int ->
    ?throw:bool ->
    ?canonize:string ->
    ?boltz_evaluated:Runtime.Type.def_type list * Runtime.Type.func * float ->
    Format.formatter ->
    float ->
    int ->
    string ->
    'b ->
    unit

  val gen_test :
    ?out_err:bool ->
    ?ftotest:string ->
    ?tsrange:int * int ->
    ?boltz_evaluated:Runtime.Type.def_type list * Runtime.Type.func * float ->
    Format.formatter ->
    float ->
    int ->
    string ->
    unit

  val gen_to_string :
    ?throw:bool -> ?canonize:string -> Runtime.Type.func -> string

  val gen_value :
    ?tsrange:int * int ->
    boltz_evaluated:Runtime.Type.def_type list * Runtime.Type.compo_type * float ->
    Format.formatter ->
    float ->
    int ->
    unit

  val gen_test_direct :
    ?out_err:bool ->
    ?throw:bool ->
    ?canonize:string ->
    ?boltz_evaluated:Runtime.Type.def_type list * Runtime.Type.func * float ->
    Format.formatter ->
    float ->
    int ->
    string ->
    unit

  val gen_test_diff :
    ?out_err:bool ->
    ?tsrange:int * int ->
    ?throw:bool ->
    ?canonize:string ->
    ?boltz_evaluated:Runtime.Type.def_type list * Runtime.Type.func * float ->
    string ->
    string ->
    Format.formatter ->
    float ->
    int ->
    string ->
    unit

  val gen_test_t : ?out_err:bool -> float -> int -> string -> unit
  (** Simple test generation function*)

  val gen_test_d :
    ?throw:bool -> ?canonize:string -> float -> int -> string -> unit

  val gen : ?out_err:bool -> float -> int -> string -> unit
  val gen_dir : float -> int -> string -> unit
end

module Gen_for_caseine : sig
  val copy_file : Format.formatter -> string -> unit

  val gen_header :
    ?print_base:bool ->
    float ->
    Format.formatter ->
    out_err:bool ->
    int ->
    Runtime.Type.func ->
    (Format.formatter -> unit -> unit) ->
    string ->
    (Format.formatter -> unit -> unit) ->
    unit

  val gen_case :
    Format.formatter ->
    out_err:bool ->
    ?throw:bool ->
    ?canonize:string ->
    string ->
    unit

  val gen_xml :
    ?vplid:string ->
    Format.formatter ->
    (Format.formatter -> unit -> unit) ->
    (Format.formatter -> unit -> unit) ->
    unit

  val gen_consigne : ?is_rec:bool -> Runtime.Type.func -> string
  val gen_consigne_base : Format.formatter -> unit -> unit
  val get_consigne : unit -> string
  val get_base : unit -> string

  val gen_test2 :
    ?ftotest:string ->
    ?tsrange:int * int ->
    ?boltz_evaluated:Runtime.Type.def_type list * Runtime.Type.func * float ->
    Format.formatter ->
    float ->
    int ->
    string ->
    unit

  val gen_test :
    ?ftotest:string ->
    ?tsrange:int * int ->
    ?boltz_evaluated:Runtime.Type.def_type list * Runtime.Type.func * float ->
    Format.formatter ->
    float ->
    int ->
    string ->
    unit
end
