open Type

let abstract_impl = Name ("nat", [])

(* print the to_string *)
let rec gen_string_of_compo meml = function
  | Abstract _ -> gen_string_of_compo meml abstract_impl
  | Name (x, _) as bt ->
      let nt = find_type x in
      nt.string_of_named gen_string_of_compo meml bt
  | Prod l ->
      let def, body = print_prod_aux (gen_string_of_compo meml) 1 l in
      Printf.sprintf "(fun (%s) -> (\"(\"^%s^\")\"))" def body
  | _ -> failwith "not implemented yet"

let boltzman_memoize = Hashtbl.create 10
let add_memoize bt z v = Hashtbl.add boltzman_memoize (bt, z) v

(*Hashtbl.iter
  (fun (bt, z) _ ->
    Printf.printf "Hashtblentry: '%s':%s \n" (string_of_compo bt)
      (string_of_float z))
  boltzman_memoize*)

let rec boltzman_from_compo bt x =
  if !verbose > 5 then
    Format.printf "boltzman_from_compo %a@." (pp_compo ~use_boltz_id:false) bt;
  match Hashtbl.find_opt boltzman_memoize (bt, x) with
  | Some b -> b
  | None ->
      let b =
        match bt with
        | Abstract _ -> (x, 1.0)
        | Name (n, _) ->
            let nt = find_type n in
            nt.boltzman_fun boltzman_from_compo bt x
        | Prod l ->
            let rec aux i = function
              | [] -> (1.0, 0.0)
              | [ b ] -> boltzman_from_compo b x
              | tb :: q ->
                  let v, dv = aux (i + 1) q in
                  let v2, dv2 = boltzman_from_compo tb x in
                  (v *. v2, (dv *. v2) +. (dv2 *. v))
            in
            aux 1 l
        | _ -> (x, 1.0)
      in
      add_memoize bt x b;
      b

let rec equations_from_compo bt accrec =
  match bt with
  | Name (n, _) ->
      let nt = find_type n in
      if not nt.is_simple then nt.get_equ equations_from_compo bt accrec
      else accrec
  | Prod l ->
      let b =
        List.fold_left (fun acc b -> equations_from_compo b acc) accrec l
      in
      b
  | _ -> accrec

(* generate the value as a value *)
let rec gen_from_compo rs m bt z =
  match bt with
  | Abstract _ ->
      let v, s = gen_from_compo rs m abstract_impl z in
      (hide (reveal v abstract_impl) bt, s)
      (* Cast int -> abstract *)
  | Name (x, _) ->
      let nt = find_type x in
      let v, s = nt.gen_fun gen_from_compo boltzman_from_compo rs m bt z in
      (v, s)
  (* | Prod [ t ] -> gen_from_compo rs m t z*)
  | Prod l ->
      let n = List.length l in
      let arrayt = Array.make n 0 in
      let size = ref 1 in
      List.iteri
        (fun i ct ->
          let v, s = gen_from_compo rs m ct z in
          size := !size + s;
          let value = reveal v ct in
          arrayt.(i) <- value)
        l;
      (hide arrayt bt, !size)
  | Fun (_, b) ->
      let f x =
        let v =
          Hashtbl.seeded_hash (Random.State.bits rs)
            (Marshal.to_bytes x [ Marshal.Closures ])
        in
        let rs2 = Random.State.make [| v |] in
        (*Name (t, []) in*)
        let vr, _ = gen_from_compo rs2 m b z in
        reveal vr b
      in
      (hide f bt, 1)

(* print the generated value as string *)
let rec print_from_compo bt f cv =
  let open Format in
  match bt with
  | Abstract _ ->
      (* Abstract are instantiate as int *)
      pp_print_int f (reveal cv bt)
  | Name (x, _) ->
      let nt = find_type x in
      Format.pp_open_stag f (Named_type_stag bt);
      nt.print print_from_compo bt f cv;
      Format.pp_close_stag f ()
  (*  | Prod [ t ] -> print_from_compo t f cv*)
  | Prod l ->
      let vtable = reveal cv bt in
      let ttable = Array.of_list l in
      let n = Array.length vtable in
      if n > 1 then Format.pp_open_stag f (Prod_stag bt);
      fprintf f "@[(";
      Array.iteri
        (fun i v ->
          let t = ttable.(i) in
          if i > 0 then fprintf f ",@,";
          print_from_compo t f (hide v t))
        vtable;
      fprintf f ")@]";
      if n > 1 then Format.pp_close_stag f ()
  | _ -> failwith "not implemented yet print_from_compo"

let named_of_alias name arg ct =
  let inst bt = instantiate_named name arg bt ct in
  {
    identifier = name;
    boltz_identifier = name;
    is_simple = false;
    arguments = List.length arg;
    get_equ = (fun _ bt acc -> equations_from_compo (inst bt) acc);
    gen_fun = (fun _ _ rs m bt z -> gen_from_compo rs m (inst bt) z);
    print = (fun _ bt f v -> print_from_compo (inst bt) f v);
    string_of_named = (fun _ rdp bt -> gen_string_of_compo rdp (inst bt));
    boltzman_fun = (fun _ bt -> boltzman_from_compo (inst bt));
  }

let named_of_def = function
  | a, b, Sum s -> Sum_type.named_of_sum a b s
  | a, b, Alias c -> named_of_alias a b c
  | a, b, Record c -> Record_type.to_named a b c

let named_of_string st = named_of_def @@ Parse_from_compiler.parse_typedef st

let evaluate st =
  let named = named_of_def st in
  add_type_to_lib named

let stagfun_struct =
  let open Format in
  (*set_tags true;*)
  let cid = ref 0 in
  let heap = ref [] in
  let heap2 = ref [] in

  let print_parent t id q =
    let edge =
      match q with [] -> "" | t2 :: _ -> sprintf "\nN%i -> N%i;//" t2 id
    in
    sprintf "\"] N%i;%s" t edge
  in

  let is_parent_prod st =
    match (st, !heap2) with
    | Named_type_stag _, _ -> 2
    | _, Named_type_stag _ :: _ -> 0
    | _, Prod_stag _ :: Named_type_stag _ :: _ -> 1
    | _ -> 2
  in

  (* let pre_is_parent () =
       match !heap with t :: _ when !cid - 1 = t -> true | _ -> false
     in*)
  {
    print_open_stag = (fun _ -> ());
    print_close_stag = (fun _ -> ());
    mark_close_stag =
      (function
      | (Named_type_stag _ | Prod_stag _) as st when is_parent_prod st >= 2 ->
          let s =
            match !heap with
            | t :: h2 when !cid - 1 = t -> print_parent t (!cid - 1) h2
            | _ -> ""
          in
          heap := List.tl !heap;
          heap2 := List.tl !heap2;
          s
      | Prod_stag _ ->
          heap2 := List.tl !heap2;
          ""
      | Graph_stag -> "}"
      | _ -> "");
    mark_open_stag =
      (function
      | (Named_type_stag _ | Prod_stag _) as st when is_parent_prod st >= 1 ->
          let pre =
            match !heap with
            | t :: q when t = !cid - 1 -> print_parent t (!cid - 1) q
            | _ -> ""
          in
          heap := !cid :: !heap;
          heap2 := st :: !heap2;
          (*let id = !cid in*)
          incr cid;
          sprintf "%s\nnode [%slabel=\"" pre
            (match st with
            | Prod_stag _ -> "shape=box,"
            | _ -> "shape=ellipse,")
      | Prod_stag st ->
          heap2 := Prod_stag st :: !heap2;
          ""
      | Graph_stag -> "digraph G{"
      | _ ->
          ""
          (*print_open_stag =
            (function String_tag x -> print_endline x | _ -> ());*));
  }

let gen_compo_type hash rs m z = function
  | Fun (l, ofun) ->
      let n = Random.int m in
      let fid = Hashtbl.find hash (l, ofun) in
      (Format.sprintf "(fun_%c %i)" fid n, 1)
  | ct ->
      let cv, size = gen_from_compo rs m ct z in
      (*Format.printf "got:%s@." (magic_print (reveal cv ct));*)
      let ft = Format.str_formatter in
      print_from_compo ct ft cv;
      (Format.flush_str_formatter (), size)

let already_gen = Hashtbl.create 10

let rec call_random ?tsrange ?(max_iter = 100) hash rs m z f =
  let m2 = List.map (fun t -> (t, gen_compo_type hash rs m z t)) f.intypes in
  let m3, size =
    List.fold_left
      (fun (acc, s1) (t, (s, s2)) ->
        ( (match t with Name _ -> acc ^ " (" ^ s ^ ")" | _ -> acc ^ " " ^ s),
          s1 + s2 ))
      ("", 0) m2
  in
  let str = Printf.sprintf "%s%s" f.name m3 in
  if max_iter <= 0 then str
  else if Hashtbl.mem already_gen str then
    call_random ?tsrange ~max_iter:(max_iter - 1) hash rs m z f
  else (
    Hashtbl.add already_gen str ();
    match tsrange with
    | None -> str
    | Some (low, up) when low <= size && size <= up -> str
    | _ -> call_random ?tsrange ~max_iter:(max_iter - 1) hash rs m z f)
