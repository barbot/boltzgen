open Type
open Format

let rec print_rec_aux f inst = function
  | [] -> ""
  | [ (n, b) ] -> Printf.sprintf "(%s r.%s)" (f (inst b)) n
  | (tn, tb) :: q ->
      let body = print_rec_aux f inst q in
      Printf.sprintf "(%s r.%s)^\"; \"^%s" (f (inst tb)) tn body

let to_named name arg r =
  let to_prod bt =
    instantiate_named name arg bt (Prod (List.map (fun (_, t) -> t) r))
  in

  {
    identifier = name;
    boltz_identifier = name;
    is_simple = false;
    arguments = List.length arg;
    get_equ = (fun f bt acc -> f (to_prod bt) acc);
    gen_fun = (fun f _ rs m bt z -> f rs m (to_prod bt) z);
    print =
      (fun f bt formatter v ->
        let cbt = to_prod bt in
        let vtable = reveal v cbt in
        let ttable = Array.of_list r in
        fprintf formatter "@[{";
        Array.iteri
          (fun i v ->
            let nt, t = ttable.(i) in
            let instt = instantiate_named name arg bt t in
            if i > 0 then fprintf formatter ";@ ";
            fprintf formatter "%s = %a" nt (f instt) (hide v instt))
          vtable;
        fprintf formatter "}@]");
    string_of_named =
      (fun f rdp bt ->
        let inst = instantiate_named name arg bt in
        let body = print_rec_aux (f rdp) inst r in
        Printf.sprintf "(fun r -> (\"{\"^%s^\"}\"))" body);
    boltzman_fun = (fun f bt -> f (to_prod bt));
  }
