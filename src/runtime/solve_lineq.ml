open Type
open Math

let print_t f t = Format.fprintf f "B(%a)" (fun f _ -> pp_compo f t) t

let print_poly_b f l =
  let open Format in
  if l = [] then fprintf f "0"
  else
    List.iter
      (fun ((tl, n), a) ->
        fprintf f "+";
        if a <> 1.0 then fprintf f "%g" a;
        if n = 1 then fprintf f "z";
        if n > 1 then fprintf f "z^%i" n;
        match tl with
        | [] -> ()
        | [ t ] -> fprintf f "%a" print_t t
        | l -> List.iter (fun t -> print_t f t) l)
      l

let print_equations f eqs =
  Array.iter
    (fun (t, p) ->
      Format.fprintf f "%a = " print_t t;
      print_poly_b f p;
      print_newline ())
    eqs

let instantiate_in_z f z eqs =
  (*iter on equation*)
  List.map
    (fun (bt, p) ->
      (* iter on monome *)
      let p2 =
        List.fold_left
          (fun acc ((tl, n), x) ->
            (* iter on type *)
            let m =
              List.fold_left
                (fun (acc2, x2) t ->
                  match f t z with
                  | None -> (t :: acc2, x2)
                  | Some (tx, _) ->
                      (* need to derive the product *)
                      (acc2, x2 *. tx))
                ([], x *. (z ** float n))
                tl
            in
            Math.sort_add_poly m acc)
          [] p
      in
      (bt, p2))
    eqs

let diff_z_product f z xB (tl, n) w =
  let nf = float n in
  List.fold_left
    (fun (pacc, ca) t ->
      match f t z with
      | None ->
          (* the type is a sum type to be solve *)
          let c = List.assoc t xB in
          (sort_add_poly ([ t ], ca) (mult_poly c pacc), c *. ca)
      | Some (x, dx) ->
          (* the type is a simple type of weight x *)
          (sort_add_poly ([], dx *. ca) (mult_poly x pacc), x *. ca))
    ([ ([], w *. nf *. (z ** (nf -. 1.0))) ], w *. (z ** nf))
    tl

let instantiate_in_diff_z f z eqs xB =
  (*iter on equation*)
  List.map
    (fun (bt, p) ->
      (* iter on monome *)
      let p2 =
        List.fold_left
          (fun acc (term, p) ->
            (* iter on type *)
            let df, _ = diff_z_product f z xB term p in
            add_poly acc df)
          [] p
      in
      (bt, p2))
    eqs

let diff_in_type p t =
  p
  |> List.map (fun (tl, c) ->
         ( List.fold_left
             (fun (n, acc) t2 ->
               if t2 = t && n = 0 then (1, acc)
               else if t2 = t then (n + 1, t :: acc)
               else (n, t2 :: acc))
             (0, []) tl,
           c ))
  |> List.filter (fun ((n, _), _) -> n > 0)
  |> List.map (fun ((n, tl), c) -> (tl, float n *. c))

let map_of_equations eqs =
  snd
  @@ Array.fold_left (fun (i, l) (eq, _) -> (i + 1, (eq, i) :: l)) (0, []) eqs

let ap_poly map p x =
  List.fold_left
    (fun acc (tl, v) ->
      acc
      +. v
         *. List.fold_left (fun accm ct -> accm *. x.(List.assoc ct map)) 1.0 tl)
    0.0 p

let compute_jacobian eqs =
  let n = Array.length eqs in
  let jac = Array.make_matrix n n [] in
  for i = 0 to n - 1 do
    for j = 0 to n - 1 do
      let td = fst eqs.(j) in
      jac.(i).(j) <- diff_in_type (snd eqs.(i)) td
      (*if !verbose > 4 then
        Printf.printf "d%a/d%a = %a\n" print_t
          (fst eqs.(i))
          print_t td print_poly_b
          jac.(i).(j)*)
    done
  done;
  jac

let comp_dx eqs jac map x =
  let jacx = Array.map (fun a -> Array.map (fun b -> ap_poly map b x) a) jac in
  let fx = Array.map (fun (_, p) -> ap_poly map p x) eqs in
  let dx = Math.solve jacx (Array.map ( ~-. ) fx) in
  if !verbose > 4 then (
    Printf.printf "jac:[%a]\n" Math.print_mat jacx;
    (*print_equations stdout eqs;*)
    Printf.printf "x\tfx\tdx[\n%a]\n" Math.print_vec_list [ x; fx; dx ]);
  (fx, dx)

let solve_eq equations z =
  (*if !verbose > 4 then print_equations stdout equations;*)
  let jac = compute_jacobian equations in
  let map = map_of_equations equations in
  let xB =
    Math.newton_raphson_multivariate
      (comp_dx equations jac map)
      (Array.make (Array.length jac) z)
  in
  List.map (fun (bt, i) -> (bt, xB.(i))) map

let compute_b_sum f eqrec z =
  let equations =
    instantiate_in_z f z eqrec
    (*|> List.fold_left
         (fun acc (bt, p) -> ((bt, false), p) :: ((bt, true), p) :: acc)
         []*)
    |> List.map (fun (bt, p) -> (bt, ([ bt ], -1.0) :: p))
    |> Array.of_list
  in
  solve_eq equations z

let compute_b_diff f eqrec z xB =
  let equations =
    instantiate_in_diff_z f z eqrec xB
    (*|> List.fold_left
      (fun acc (bt, p) -> ((bt, false), p) :: ((bt, true), p) :: acc)
      []*)
    |> List.map (fun (bt, p) -> (bt, ([ bt ], -1.0) :: p))
    |> Array.of_list
  in
  solve_eq equations z

let compute_size f eq z =
  try
    let xB = compute_b_sum f eq z in
    if !verbose > 2 then
      List.iter
        (fun (ct, x) -> Format.printf "z:%g -> %a = %g@." z print_t ct x)
        xB;

    (*let x, dx = boltzman_from_compo intype z in
      if x > 1.0e+100 then (nan, 0.0) else (z *. dx /. x, 0.0)*)
    if List.exists (fun (_, v) -> v <= 0.0 || classify_float v <> FP_normal) xB
    then None
    else
      let dxB = compute_b_diff f eq z xB in
      Some
        (List.map2
           (fun (t, v) (t2, v2) ->
             assert (t = t2);
             (t, v, v2))
           xB dxB)
  with Math.Matrix_singular -> None
