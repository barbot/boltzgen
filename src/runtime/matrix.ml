type matrix = float array array

let iteri f = Array.iteri (fun i x -> Array.iteri (fun j y -> f i j y) x)

let make_mat m t =
  let m2 = Array.make (Array.length m) [| t |] in
  Array.iteri (fun i _ -> m2.(i) <- Array.make (Array.length m.(i)) t) m;
  m2

let dmat = [||]
let id3 = [| [| 1.; 0.; 0. |]; [| 0.; 1.; 0. |]; [| 0.; 0.; 1. |] |]
let nulln n = Array.make_matrix n n 0.

let idn n =
  let a = nulln n in
  for i = 0 to n - 1 do
    a.(i).(i) <- 1.
  done;
  a

let base_vect i n =
  let a = Array.make n 0. in
  a.(i) <- 1.;
  a

let print_matrix a =
  print_string "[|";
  for i = 0 to Array.length a - 1 do
    if i <> 0 then print_string "\n     ";
    print_string "[| ";
    for j = 0 to Array.length a.(0) - 1 do
      if j <> 0 then print_string ";";
      print_float a.(i).(j)
    done;
    print_string "|]"
  done;
  print_string "|]"

let glue_matrixh a b =
  if Array.length a <> Array.length b then
    failwith
      (Printf.sprintf "matrix error glueh %i %i" (Array.length a)
         (Array.length b));
  let c =
    Array.make_matrix (Array.length a)
      (Array.length a.(0) + Array.length b.(0))
      0.
  in
  for i = 0 to Array.length a - 1 do
    c.(i) <- Array.append a.(i) b.(i)
  done;
  c

let glue_matrixv a b = Array.append a b

let glue_matrixv2 a b =
  if Array.length a.(0) <> Array.length b.(0) then
    failwith
      (Printf.sprintf "matrix error glueh %i %i"
         (Array.length a.(0))
         (Array.length b.(0)));
  Array.append a b

let extract_matrix a n m k l =
  let b = Array.make_matrix k l 0. in
  for i = 0 to k - 1 do
    for j = 0 to l - 1 do
      b.(i).(j) <- a.(n + i).(m + j)
    done
  done;
  b

let add_matrix a b =
  if
    Array.length a <> Array.length b || Array.length a.(0) <> Array.length b.(0)
  then
    failwith
      (Printf.sprintf "matrix error add %i %i %i %i" (Array.length a)
         (Array.length b)
         (Array.length a.(0))
         (Array.length b.(0)));
  let c = Array.make_matrix (Array.length a) (Array.length a.(0)) 0. in
  for i = 0 to Array.length a - 1 do
    for j = 0 to Array.length a.(0) - 1 do
      c.(i).(j) <- a.(i).(j) +. b.(i).(j)
    done
  done;
  c

let add_stable_matrix a b =
  for i = 0 to Array.length a - 1 do
    for j = 0 to Array.length a.(0) - 1 do
      a.(i).(j) <- a.(i).(j) +. b.(i).(j)
    done
  done

let sous_matrix a b =
  if
    Array.length a <> Array.length b || Array.length a.(0) <> Array.length b.(0)
  then
    failwith
      (Printf.sprintf "matrix error sub %i %i %i %i" (Array.length a)
         (Array.length b)
         (Array.length a.(0))
         (Array.length b.(0)));
  let c = Array.make_matrix (Array.length a) (Array.length a.(0)) 0. in
  for i = 0 to Array.length a - 1 do
    for j = 0 to Array.length a.(0) - 1 do
      c.(i).(j) <- a.(i).(j) -. b.(i).(j)
    done
  done;
  c

let moins_matrix a =
  let c = Array.make_matrix (Array.length a) (Array.length a.(0)) 0. in
  for i = 0 to Array.length a - 1 do
    for j = 0 to Array.length a.(0) - 1 do
      c.(i).(j) <- -.a.(i).(j)
    done
  done;
  c

let mult_matrix a b =
  if Array.length a.(0) <> Array.length b then
    failwith
      (Printf.sprintf "matrix error Mult %i %i"
         (Array.length a.(0))
         (Array.length b));
  let c = Array.make_matrix (Array.length a) (Array.length b.(0)) 0. in
  for i = 0 to Array.length a - 1 do
    for j = 0 to Array.length b.(0) - 1 do
      for k = 0 to Array.length b - 1 do
        c.(i).(j) <- c.(i).(j) +. (a.(i).(k) *. b.(k).(j))
      done
    done
  done;
  c

let mult_spe a b =
  let c = Array.make_matrix 1 (Array.length b.(0)) 0. in
  let n0 = Array.length b - 1 in
  let n1 = Array.length b.(0) - 1 in
  for j = 0 to n1 do
    for k = 0 to n0 do
      c.(0).(j) <- c.(0).(j) +. (a.(0).(k) *. b.(k).(j))
    done
  done;
  for j = n1 + 1 to Array.length a.(0) - 1 do
    c.(0).(j) <- a.(0).(j);
    for k = 0 to n0 do
      c.(0).(j) <- c.(0).(j) +. (a.(0).(k) *. b.(k).(j))
    done
  done;
  c

let mult_real_matrix k a =
  let c = Array.make_matrix (Array.length a) (Array.length a.(0)) 0. in
  for i = 0 to Array.length a - 1 do
    for j = 0 to Array.length a.(0) - 1 do
      c.(i).(j) <- a.(i).(j) *. k
    done
  done;
  c

let mult_real_stable_matrix k a =
  for i = 0 to Array.length a - 1 do
    for j = 0 to Array.length a.(0) - 1 do
      a.(i).(j) <- a.(i).(j) *. k
    done
  done

let transpose a =
  let b = Array.make_matrix (Array.length a.(0)) (Array.length a) 0. in
  for i = 0 to Array.length a - 1 do
    for j = 0 to Array.length a.(0) - 1 do
      b.(j).(i) <- a.(i).(j)
    done
  done;
  b

let diag_matrix a =
  let n = Array.length a in
  let b = Array.make_matrix n n 0. in
  for i = 0 to n - 1 do
    b.(i).(i) <- a.(i)
  done;
  b

let pvint (x1, y1) (x2, y2) = (x1 * y2) - (y1 * x2)
let psint (x1, y1) (x2, y2) = (x1 * x2) + (y1 * y2)
let moinspt (x1, y1) (x2, y2) = (x1 - x2, y1 - y2)

let inter2 a b c d =
  let orig = moinspt b a in
  pvint (moinspt c a) orig * pvint (moinspt d a) orig

let inter a b c d = if inter2 a b c d <= 0 && inter2 c d a b <= 0 then 1 else 0
let matrix_of_vect a = transpose [| a |]

let vect_of_matrix a =
  let a2 = transpose a in
  a2.(0)

let vects_of_matrix a =
  ( [| [| a.(0).(0) |]; [| a.(1).(0) |]; [| a.(2).(0) |] |],
    [| [| a.(0).(1) |]; [| a.(1).(1) |]; [| a.(2).(1) |] |],
    [| [| a.(0).(2) |]; [| a.(1).(2) |]; [| a.(2).(2) |] |] )

let ( +.. ) a b = add_matrix a b
let ( -.. ) a b = sous_matrix a b
let ( *.. ) a b = mult_matrix a b
let ( *-. ) a b = mult_real_matrix a b
