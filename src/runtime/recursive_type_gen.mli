(** {1 Functions for handling value generation either directly or by calling their generotor for name type} *)

val evaluate : Type.def_type -> unit
(** Build the type and add it to the type librairy *)

val named_of_string : string -> Type.named_type
(** Parse and build a type definition *)

val gen_string_of_compo : Type.recdefprint -> Type.compo_type -> string
(** Generate the to_string function for a type *)

val add_memoize : Type.compo_type -> float -> float * float -> unit

val equations_from_compo : Type.equ_gen_type
(** return all boltzmann equations *)

val boltzman_from_compo : Type.boltzmann_generator
(** Evaluate the boltzmann function for a type *)

val gen_from_compo : Type.value_generator
(** Generate a value for a type *)

val print_from_compo : Type.printer_type
(** Print a value given its type *)

val call_random :
  ?tsrange:int * int ->
  ?max_iter:int ->
  (Type.compo_type list * Type.compo_type, char) Hashtbl.t ->
  Random.State.t ->
  int ->
  float ->
  Type.func ->
  string
(** Generate and print a call for a function *)

val stagfun_struct : Format.formatter_stag_functions
