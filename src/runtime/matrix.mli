(** Implementation of a matrix library *)

(** Matrix have the data structure of an array of array of float. *)

type matrix = float array array
(** base type for matrix *)

val iteri : (int -> int -> 'a -> unit) -> 'a array array -> unit
(** Iterator on matrix: [Matrix.iteri f m] apply f to each element of the matrix*)

val make_mat : 'a array array -> 'b -> 'b array array
(** [Matrix.make_mat m b] returns a fresh matrix of the same size than [m] contening
b in each value*)

val dmat : matrix
(** Empty matrix *)

val id3 : matrix
(** Identity matrix of size 3x3 *)

val nulln : int -> matrix
(** [Matrix.nulln n] returns a matrix of size nxn fill with [0.] *)

val idn : int -> matrix
(** [Matrix.idn n] returns the identity matrix of size nxn *)

val base_vect : int -> int -> float array
(** [Matrix.base_vect i n] returns an array of size n fill with [0.] except at the position [i] cointening [1.]*)

val print_matrix : matrix -> unit
(** Output the matrix on the standart output*)

val glue_matrixh : matrix -> matrix -> matrix
(** [Matrix.glue_matrixh a b] return a fresh matrix contening the concatenation of a and b by row*)

val glue_matrixv : 'a array -> 'a array -> 'a array
(** [Matrix.glue_matrixh a b] return a fresh matrix contening the concatenation of a and b by column*)

val glue_matrixv2 : matrix -> matrix -> matrix

val extract_matrix : matrix -> int -> int -> int -> int -> matrix
(** [Matrix.extract_matrix m i j k l] returns a fresh matrix contening the element
    of [m] from indice [i] [j] of size [k] x [l]*)

val add_matrix : matrix -> matrix -> matrix
(** [Matrix.add_matrix a b] returns a fresh matrix contening [a] plus [b].
[a] and [b] should have the same size*)

val add_stable_matrix : matrix -> matrix -> unit
(** [Matrix.add_stable_matrix a b] compute the matrix [a] plus [b] inside [a]*)

val sous_matrix : matrix -> matrix -> matrix
(** [Matrix.sous_matrix a b] returns a fresh matrix contening [a] minus [b].
[a] and [b] should have the same size*)

val moins_matrix : matrix -> matrix
(** returns the oposite matrix for the addition*)

val mult_matrix : matrix -> matrix -> matrix
(** [Matrix.mult_matrix a b] returns a fresh matrix contening the product of [a] and [b].
[a] and [b] should have the corresonding size*)

val mult_spe : matrix -> matrix -> matrix

val mult_real_matrix : float -> matrix -> matrix

val mult_real_stable_matrix : float -> matrix -> unit
(** [Matrix.mult_real_stable_matrix f a] compute the matrix [f] * [a] inside [a]*)

val transpose : matrix -> matrix

val diag_matrix : float array -> matrix

val pvint : int * int -> int * int -> int

val psint : int * int -> int * int -> int

val moinspt : int * int -> int * int -> int * int

val inter2 : int * int -> int * int -> int * int -> int * int -> int

val inter : int * int -> int * int -> int * int -> int * int -> int

val matrix_of_vect : float array -> matrix

val vect_of_matrix : matrix -> float array

val vects_of_matrix : matrix -> matrix * matrix * matrix

val ( +.. ) : matrix -> matrix -> matrix

val ( -.. ) : matrix -> matrix -> matrix

val ( *.. ) : matrix -> matrix -> matrix

val ( *-. ) : float -> matrix -> matrix
