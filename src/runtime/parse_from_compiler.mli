(* Call the compiler lib to parse the string as a signature, then convert it to Type *)

val parse_string : string -> Type.def_type list * Type.func option

val parse_typedef : string -> Type.def_type
