val verbose : int ref

(** A type for ocaml type*)
type compo_type =
  | Name of string * compo_type list
      (** denote a [type expr] with arguments i.e. ['a list] *)
  | Abstract of string  (** denote a polymorphic type i.e. ['a] *)
  | Fun of compo_type list * compo_type
      (** denote a function i.e. ['a -> 'b] *)
  | Prod of compo_type list  (** denote a tupe i.e. [ 'a * 'b * ... 'z ] *)

type func = { name : string; intypes : compo_type list; outtype : compo_type }
(** A type for named function *)

val flatten_prod : compo_type list -> compo_type list
val ano_func : func -> compo_type
val deano_func : string -> compo_type -> func

val instantiate_named :
  string -> string list -> compo_type -> compo_type -> compo_type
(**
   [instantiate_named name args concete_named vtype] Instantiate [vtype] by replacing abstract type in the [args] list by its types given as a named type.
   Example with [int option] : [instantiate_named "option" ["a"] (Name ("option",[Name ("int",[])])) (Abstract "a")] returns [ Name ("int",[])] 
*)

type sum_type_def = (string * compo_type option * float option) list

type def_type_elem =
  | Sum of sum_type_def
  | Alias of compo_type
  | Record of (string * compo_type) list

type def_type = string * string list * def_type_elem
(** define a named type with name, list of argument and definition *)

(** {1 Dynamic typing} *)

type hidden_type
(** Poor man dynamic typing. See [hide] and [reveal] for mor detail. Usefull for value generation at runtime.
*)

val hide : 'a -> compo_type -> hidden_type
(** Hide a value inside [hidden_type] the value could be only read by
    [reveal] the [compo_type] serve as a guard to ensure type corectess.
    reveal assert that the two types matches.
    for a [compo_type] [t] the composition [reveal (hide x t) t] act like [Obj.magic x] use with caution. *)

val reveal : hidden_type -> compo_type -> 'a
(** See [hide] *)

(** {1 Generic type for a type with sampler}*)

type gen_function = float -> float * float
type recdefprint = (compo_type * string) list
type poly = ((compo_type list * int) * float) list
type poly_assoc = (compo_type * poly) list
type equ_gen_type = compo_type -> poly_assoc -> poly_assoc
type printer_type = compo_type -> Format.formatter -> hidden_type -> unit
type gen_to_string = recdefprint -> compo_type -> string

type value_generator =
  Random.State.t -> int -> compo_type -> float -> hidden_type * int
(** Type for a value generator given a random generator state a maximal value, a type [t] a boltzmann parameter, generate a hidden OCaml value and its size of type [t]*)

type boltzmann_generator = compo_type -> gen_function

type named_type = {
  identifier : string;  (** Name of the type as OCaml name it *)
  boltz_identifier : string;
      (** Name of the type as Boltzgen name it for example nat instead of int for natural number*)
  is_simple : bool;  (** is define recursively *)
  arguments : int;  (** Number of arguments *)
  get_equ : equ_gen_type -> equ_gen_type;
      (** Return the set of equations require for boltzmann sampling *)
  gen_fun : value_generator -> boltzmann_generator -> value_generator;
      (** Generate a hidden OCaml value and its size from a type*)
  print : printer_type -> printer_type;
      (** Given a hidden value, print it on the formatter *)
  string_of_named : gen_to_string -> gen_to_string;
      (** Print the printing function for this type *)
  boltzman_fun : boltzmann_generator -> boltzmann_generator;
      (** Compute the boltzmann generating function *)
}
(** Datastructure for sampler of named type*)

(** {1 Types librairy} *)

val find_type : string -> named_type
val add_type_to_lib : ?rename:string -> named_type -> unit

(*val string_of_compo : ?use_boltz_id:bool -> compo_type -> string*)

(** {1 Pretty printer} *)

type Format.stag +=
  | Named_type_stag of compo_type
  | Prod_stag of compo_type
  | Graph_stag

val pp_compo : ?use_boltz_id:bool -> Format.formatter -> compo_type -> unit
val pp_type_of_out : Format.formatter -> compo_type -> unit

(*val print_func : out_channel -> func -> unit*)

val pp_func :
  ?use_boltz_id:bool -> ?pval:bool -> Format.formatter -> func -> unit

val pp_def : Format.formatter -> def_type -> unit
val print_prod_aux : ('a -> string) -> int -> 'a list -> string * string

(*val magic_print : 'a -> string
  (** Print anything as a tree, very low level*)
*)
