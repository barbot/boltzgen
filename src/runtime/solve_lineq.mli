val print_equations :
  Format.formatter -> (Type.compo_type * Type.poly) array -> unit

val compute_size :
  (Type.compo_type -> float -> (float * float) option) ->
  Type.poly_assoc ->
  float ->
  (Type.compo_type * float * float) list option
