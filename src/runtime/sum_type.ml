open Type
open Math

let named_of_sum =
  let count = ref 1 in
  fun name args (ts_abstr : sum_type_def) ->
    (* Instantiate abstract type to concrete one *)
    let instconstr_constr constr_content bt =
      match constr_content with
      | None -> None
      | Some v -> Some (instantiate_named name args bt v)
    in

    let compute_boltz_constr f bt z (_, ct2, w2) =
      let w = match w2 with Some x -> x | _ -> 1.0 in
      let ct = instconstr_constr ct2 bt in
      match ct with
      | None -> (0, w *. z)
      (* | Some (Prod tl) ->
           List.fold_left
             (fun (i, fx) tin ->
               if tin = bt then (i + 1, fx)
               else
                 let fx2, _ = f tin z in
                 (i, fx *. fx2))
             (0, w *. z)
             tl*)
      | Some ti ->
          let fx, _ = f ti z in
          (0, w *. fx *. z)
    in

    (* hack for list *)
    let rename_type = function
      | Name ("list", [ arg ]) -> Name ("m__list", [ arg ])
      | x -> x
    in

    let compute_boltz_constr_eq f bt acc (_, ct2, w2) =
      let w = match w2 with Some x -> x | _ -> 1.0 in
      let ct = instconstr_constr ct2 bt in
      match ct with
      | None -> ((([], 1), w), acc)
      | Some ti when ti = bt -> ((([ rename_type ti ], 1), w), acc)
      | Some (Prod tl) ->
          let tl2 = flatten_prod tl in
          List.fold_left
            (fun (((i, n), fx), acc2) tin ->
              if tin = bt then (((tin :: i, n), fx), acc2)
              else
                let acc3 = f tin acc2 in
                (((rename_type tin :: i, n), fx), acc3))
            ((([], 1), w), acc)
            tl2
      | Some ti ->
          let acc2 = f ti acc in
          ((([ rename_type ti ], 1), w), acc2)
    in

    let index_constr l c =
      let rec aux (i, j) = function
        | [] -> raise Not_found
        | (con, None, _) :: _ when c = con -> i
        | (_, None, _) :: q -> aux (i + 1, j) q
        | (con, Some _, _) :: _ when con = c -> j
        | (_, Some _, _) :: q -> aux (i, j + 1) q
      in
      aux (0, 0) l
    in

    let get_index l is_b i =
      let rec aux j = function
        | [] -> raise Not_found
        | ((_, Some _, _) as c) :: _ when is_b && i = j -> c
        | (_, Some _, _) :: q when is_b -> aux (j + 1) q
        | (_, Some _, _) :: q -> aux j q
        | ((_, None, _) as c) :: _ when (not is_b) && i = j -> c
        | (_, None, _) :: q when not is_b -> aux (j + 1) q
        | (_, None, _) :: q -> aux j q
      in

      aux 0 l
    in

    let eval_boltz btotal (i, x) = (btotal ** float i) *. x in

    {
      identifier = name;
      boltz_identifier = name;
      arguments = List.length args;
      is_simple = false;
      get_equ =
        (fun f bt accrec ->
          if List.exists (fun (x, _) -> x = bt) accrec then accrec
          else
            let accrec2 = (bt, []) :: accrec in
            let polysum, acc2 =
              List.fold_left
                (fun (acc_poly, acc_other) vt ->
                  let p, acc_other2 =
                    compute_boltz_constr_eq f bt acc_other vt
                  in
                  (sort_add_poly p acc_poly, acc_other2))
                ([], accrec2) ts_abstr
            in
            List.map
              (fun (x, v) -> if x = bt then (bt, polysum) else (x, v))
              acc2);
      gen_fun =
        (fun f boltz rs m bt z ->
          let p = Random.State.float rs 1.0 in
          let btotal, _ = boltz bt z in
          let const_weight =
            List.map
              (fun av ->
                let eb =
                  eval_boltz btotal (compute_boltz_constr boltz bt z av)
                in
                (av, eb /. btotal))
              ts_abstr
          in

          let rec findp pa = function
            | [] -> assert false
            | [ (t, _) ] -> t
            | (t, w) :: _ when pa +. w > p -> t
            | (_, w) :: q -> findp (pa +. w) q
          in

          let constr, ct, _ = findp 0.0 const_weight in
          let v, size =
            match instconstr_constr ct bt with
            | None ->
                let v = hide (index_constr ts_abstr constr) bt in
                (v, 1)
            | Some ti ->
                let vc, s = f rs m ti z in
                let vcr = reveal vc ti in
                if Obj.is_block vcr then
                  let nb = Obj.with_tag (index_constr ts_abstr constr) vcr in

                  (hide nb bt, s + 1)
                else
                  let nb = Obj.new_block (index_constr ts_abstr constr) 1 in
                  Obj.set_field nb 0 vcr;
                  (hide nb bt, s + 1)
          in
          (v, size));
      print =
        (fun f bt form o ->
          let obj = reveal o bt in
          let b = Obj.is_block obj in
          let constr, type_content, _ =
            get_index ts_abstr b (if b then Obj.tag obj else obj)
          in
          let open Format in
          if b then
            match instconstr_constr type_content bt with
            (*| Some (Prod [ tl ]) |*)
            | Some (Prod tl) ->
                fprintf form "%s%a" constr (f (Prod tl)) (hide obj (Prod tl))
            | Some tl -> fprintf form "%s(%a)" constr (f tl) (hide obj tl)
            | _ -> assert false
          else pp_print_string form constr);
      string_of_named =
        (fun f meml bt ->
          match List.assoc_opt bt meml with
          | None ->
              let j = Printf.sprintf "p%i" !count in
              incr count;
              let s =
                List.fold_left
                  (fun acc (cn, cc, _) ->
                    match instconstr_constr cc bt with
                    | None -> Printf.sprintf "%s | %s -> \"%s\"" acc cn cn
                    | Some (Prod x) ->
                        let def, body =
                          print_prod_aux (f ((bt, j) :: meml)) 1 x
                        in
                        Printf.sprintf "%s | %s (%s) -> \"%s(\"^(%s)^\")\"" acc
                          cn def cn body
                    | Some x ->
                        Printf.sprintf "%s | %s ( x) -> \"%s(\"^(%s x)^\")\""
                          acc cn cn
                          (f ((bt, j) :: meml) x))
                  (Printf.sprintf "(fun v -> let rec %s = function " j)
                  ts_abstr
              in
              s ^ Printf.sprintf " in %s v)" j
          | Some recf -> recf);
      boltzman_fun =
        (fun f bt z ->
          match bt with
          | Name (_, l) ->
              let bt2 = Name (name, l) in
              f bt2 z
          (*| Name ("list", l) ->
              Printf.printf "translate\n";
              f (Name ("m__list", l)) z*)
          | _ ->
              failwith
                (Format.sprintf "%f -> %i should be memoize" z
                   (Hashtbl.hash (bt, z))));
    }
